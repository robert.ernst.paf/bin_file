# bin_file
Mangling of various file formats that conveys binary information (Motorola S-Record, Intel HEX, TI-TXT and binary files)

bin_file is heavily inspeared by [bincopy](https://github.com/eerimoq/bincopy) and ported to rust.

Dealing with intel hex records was inspired by [ihex](https://github.com/martinmroz/ihex).

Dealing with SREC records was inspired by [srec](https://github.com/The6P4C/srec).

## Installation

`cargo add bin_copy`

## Example usage

## Supported Formats
 - [Intel Hex Format](https://en.wikipedia.org/wiki/Intel_HEX)
 - [SREC](https://en.wikipedia.org/wiki/SREC_(file_format))
 - [TI-TXT](https://software-dl.ti.com/codegen/docs/tiarmclang/compiler_tools_user_guide/compiler_manual/hex_utility_description/description-of-the-object-formats-stdz0792390.html)
 - [VERILOG VMEM](https://linux.die.net/man/5/srec_vmem)
 - [Extended Tektronix Object Format](https://en.wikipedia.org/wiki/Tektronix_hex_format)

## License

Licensed under either of

 * Apache License, Version 2.0
   ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.