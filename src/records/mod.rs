use std::{
    fmt::{Debug, Display},
    str::FromStr,
};
pub(crate) mod ext_tek_hex;
pub(crate) mod ihex;
pub(crate) mod srec;

use crate::Error;

/// Record trait for Datatypes having records.
/// A record means that a line of the file is representing
/// a closed information
pub trait Record: FromStr + Debug + Display {
    /// Creates the record string out of the record
    ///
    /// # Errors
    ///
    /// Different kind of errors, but only if datas in record are in that way
    /// that it can't be expressed in the standard
    fn to_record_string(&self) -> Result<String, Error>;

    /// Creates the ansi colour formated record string out of the record
    ///
    /// # Errors
    ///
    /// Different kind of errors, but only if datas in record are in that way
    /// that it can't be expressed in the standard
    fn to_pretty_record_string(&self) -> Result<String, Error>;

    /// Returns true if record string can be parsed
    ///
    /// # Parameters
    ///
    /// - `record_str`: Record string, which should be checked
    fn is_record_str_correct<S>(record_str: S) -> bool
    where
        S: AsRef<str>,
    {
        Self::from_record_string(record_str).is_ok()
    }

    /// Creates a record from given String
    ///
    /// # Parameters
    ///
    /// -`record_string`: String containing a record
    ///
    /// # Errors
    ///
    /// Differnt Errors if format or checksums are not matching
    /// and internal plausibility checks on record level are failing
    fn from_record_string<S>(record_string: S) -> Result<Self, Error>
    where
        S: AsRef<str>;
}
