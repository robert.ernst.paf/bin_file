#![warn(missing_docs)]

//! # BinFile
//!
//! Mangling of various file formats that conveys binary information
//! (Motorola S-Record, Intel HEX, TI-TXT and binary files).
//!
//! ## Supported Formats
//! - [Intel Hex Format](https://en.wikipedia.org/wiki/Intel_HEX)
//! - [SREC](https://en.wikipedia.org/wiki/SREC_(file_format))
//! - [TI-TXT](https://software-dl.ti.com/codegen/docs/tiarmclang/compiler_tools_user_guide/compiler_manual/hex_utility_description/description-of-the-object-formats-stdz0792390.html)
//! - [VERILOG VMEM](https://linux.die.net/man/5/srec_vmem)
//! - [Extended Tektronix Object Format](https://en.wikipedia.org/wiki/Tektronix_hex_format)

pub use records::Record;
#[cfg(feature = "serde")]
use serde::{Deserialize, Serialize};
use std::cmp::{max, min};
use std::collections::BTreeMap;
use std::fmt::Display;
use std::fs::{metadata, File};
use std::io::{BufRead, BufReader, Read, Write};
use std::ops::{Add, AddAssign, Range, RangeBounds};
use std::path::{Path, PathBuf};
use std::str::FromStr;

use bytesize::ByteSize;
pub use error::Error;
pub use records::ext_tek_hex::ExtTekHexRecord;
pub use records::ihex::IHexRecord;
pub use records::srec::SRecord;
use records::srec::{Address16, Address24, Address32, Count16, Count24, Data};
use regex::Captures;
use regex::Regex;
pub use segments::Segment;
use segments::Segments;

mod checksums;
mod error;
mod records;
mod segments;

const TI_TXT_BYTES_PER_LINE: usize = 16;
const EXT_TEK_HEX_BYTES_PER_LINE: u8 = 16;
const VERILOG_VMEM_BYTES_PER_LINE: usize = 16;

const PRINTABLE: &str = "0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ!\"#$%&'()*+, -./:;<=>?@[\\]^_`{|}~ ";

#[derive(Debug, PartialEq, Eq, Copy, Clone, Hash, Default)]
/// Enum defining the Address Length of SRec
/// Default value can be created with `SRecordAddressLength::default()`
pub enum SRecordAddressLength {
    /// 16-bit Address Length
    Length16,
    /// 24-bit Address Length
    Length24,
    /// 32-bit Address Length
    #[default]
    Length32,
}

#[derive(Debug, PartialEq, Eq, Copy, Clone, Hash, Default)]
/// Enum defining the intel hex format
/// Default value can be created with `IHexFormat::default()`
pub enum IHexFormat {
    /// IHex8 Format
    IHex8,
    /// IHex16 Format
    IHex16,
    /// IHex32 Format
    #[default]
    IHex32,
}

/// Struct representing a binary file
#[derive(Clone, Eq, PartialEq, Ord, PartialOrd, Hash, Debug)]
#[cfg_attr(feature = "serde", derive(Serialize, Deserialize))]
pub struct BinFile {
    header: Option<Vec<u8>>,
    execution_start_address: Option<usize>,
    segments: Segments,
    #[serde(skip_serializing)]
    cursor: usize,
}

impl BinFile {
    /// Creates a `BinFile` struct representing a binary file.
    /// ```rust
    /// use bin_file::BinFile;
    ///
    /// let binfile = BinFile::new();
    /// ```
    pub fn new() -> Self {
        BinFile {
            header: None,
            execution_start_address: None,
            segments: Segments::new(),
            cursor: 0,
        }
    }

    /// Creates a `BinFile` struct representing a binary file
    /// from a given file. It tries to guess the correct file
    /// format and reads it in.
    ///
    /// # Parameters
    ///
    /// - `file_name`: Reference to a [`Path`](std::path::Path)
    ///
    /// # Errors
    ///
    /// - `Error::UnsupportedFileFormat`: If file format can't
    ///   be determined
    pub fn from_file<P>(file_name: P) -> Result<Self, Error>
    where
        P: AsRef<Path>,
    {
        let mut binfile = BinFile {
            header: None,
            execution_start_address: None,
            segments: Segments::new(),
            cursor: 0,
        };
        binfile.add_file(file_name, false)?;
        Ok(binfile)
    }

    /// Creates a `BinFile` struct representing a binary file
    /// from multiple files. It tries to guess the currect file
    /// format for each file and reads it in.
    ///
    /// # Parameters
    ///
    /// - `file_names` Array of file names
    ///
    /// # Errors
    ///
    /// - `Error::UnsupportedFileFormat`: If file format can't
    ///   be determined
    /// - `Error::AddDataError`: If datas overlap in files, but `overwrite` was not set
    pub fn from_files<P, T>(file_names: P, overwrite: bool) -> Result<Self, Error>
    where
        P: AsRef<[T]>,
        T: AsRef<Path>,
    {
        let mut binfile = BinFile::new();
        for file_name in file_names.as_ref() {
            binfile.add_file(file_name, overwrite)?;
        }
        Ok(binfile)
    }

    /// Returns the header string
    pub fn header(&self) -> Option<&Vec<u8>> {
        self.header.as_ref()
    }

    /// Sets the header string
    ///
    /// # Parameters
    ///
    /// - `header`: Header to be set
    pub fn set_header_string<S>(&mut self, header: S)
    where
        S: Into<String>,
    {
        let str: String = header.into();
        self.header = Some(str.into());
    }

    /// Returns the execution start address
    pub fn execution_start_address(&self) -> Option<usize> {
        self.execution_start_address
    }

    /// Sets the execution start address
    ///
    /// # Parameters
    ///
    /// - `address`: Start address to be set
    pub fn set_exexution_start_address(&mut self, address: usize) {
        self.execution_start_address = Some(address);
    }

    /// The segments object. Can be used to iterate over all segments in
    /// the binary.
    ///
    /// Below is an example iterating over all segments, two in this
    /// case, and printing them.
    ///
    /// ```
    /// use bin_file::BinFile;
    /// use std::error::Error;
    ///
    /// fn main() -> Result<(), Box<dyn Error>> {
    ///     let mut binfile = BinFile::new();
    ///
    ///     binfile.add_bytes([0,1,2], Some(0), false)?;
    ///     binfile.add_bytes([3,4,5], Some(10), false)?;
    ///
    ///     for segment in binfile.segments() {
    ///         println!("{}", segment);
    ///     }
    ///     Ok(())
    /// }
    /// ```
    ///
    /// ```console
    /// Segment(address=0, data=[0, 1, 2])
    /// Segment(address=10, data=[3, 4, 5])
    /// ```
    ///
    /// All segment can be split into smaller pieces using the
    /// `chunks` function on segments.
    ///
    /// ```rust
    /// use bin_file::BinFile;
    /// use std::error::Error;
    ///
    /// fn main() -> Result<(), Box<dyn Error>> {
    ///     let mut binfile = BinFile::new();
    ///
    ///     binfile.add_bytes([0,1,2], Some(0), false)?;
    ///     binfile.add_bytes([3,4,5], Some(10), false)?;
    ///
    ///     for (address, data) in binfile.segments().chunks(Some(2), None)? {
    ///         println!("Address: {} Data: {:?}", address, data);
    ///     }
    ///     Ok(())
    /// }
    /// ```
    ///
    /// ```console
    /// Address: 0 Data: [0, 1]
    /// Address: 2 Data: [2]
    /// Address: 10 Data: [3, 4]
    /// Address: 12 Data: [5]
    /// ```
    ///
    /// Each segment can be split into smaller pieces using the
    /// `chunks()` function on a single segment.
    ///
    /// ```rust
    /// use bin_file::BinFile;
    /// use std::error::Error;
    ///
    /// fn main() -> Result<(), Box<dyn Error>> {
    ///     let mut binfile = BinFile::new();
    ///
    ///     binfile.add_bytes([0,1,2], Some(0), false)?;
    ///     binfile.add_bytes([3,4,5], Some(10), false)?;
    ///
    ///     for segment in binfile.segments() {
    ///         println!("{}", segment);
    ///         for (address, data) in segment.chunks(Some(2), None)? {
    ///             println!("Address: {} Data: {:?}", address, data);
    ///         }
    ///     }
    ///     Ok(())
    /// }
    /// ```
    ///
    /// ```console
    /// Segment(address=0, data=[0, 1, 2])
    /// Address: 0 Data: [0, 1]
    /// Address: 2 Data: [2]
    /// Segment(address=10, data=[3, 4, 5])
    /// Address: 10 Data: [3, 4]
    /// Address: 12 Data: [5]
    /// ```
    pub fn segments(&self) -> &Segments {
        &self.segments
    }

    /// Returns the list of Segments
    pub fn segments_list(&self) -> Vec<(usize, Vec<u8>)> {
        let mut list = Vec::new();
        for segment in self.segments() {
            let (address, data) = segment.get_tuple().to_owned();
            list.push((address, data.to_vec()));
        }
        list
    }

    /// Returns the overall minimum address
    pub fn minimum_address(&self) -> Option<usize> {
        self.segments.get_minimum_address()
    }

    /// Returns the overall maximum address
    pub fn maximum_address(&self) -> Option<usize> {
        self.segments.get_maximum_address()
    }

    /// Retruns chunks for all datas.
    /// See [`BinFile.segments()`](#method.segments) for examples.
    ///
    /// # Parameters
    ///
    /// - `size`: Size of a chunk
    /// - `alignment`: Alignment of the chunk to minimum address
    pub fn chunks(
        &self,
        size: Option<usize>,
        alignment: Option<usize>,
    ) -> Result<BTreeMap<usize, Vec<u8>>, Error> {
        self.segments.chunks(size, alignment)
    }

    /// Returns the value at a particular address
    /// and None if address is not present.
    ///
    /// # Parameters
    ///
    /// - `address`: Address which should be read out
    pub fn get_value_by_address(&self, address: usize) -> Option<u8> {
        self.segments.get_value_by_address(address)
    }

    /// Returns the values at a particular address range, range must be within a segment
    /// Returns None if value is not present or range is not within one segment
    ///
    /// # Parameters
    ///
    /// - `address`: Address Range
    ///
    /// # Examples
    ///
    /// ```rust
    /// use bin_file::BinFile;
    ///
    /// let mut binfile = BinFile::new();
    /// // First add some values
    /// assert!(binfile.add_bytes([0x01, 0x02, 0x03, 0x04], Some(1), false).is_ok());
    /// assert!(binfile.add_bytes([0x01, 0x02, 0x03, 0x04], Some(11), false).is_ok());
    ///
    /// // Read out values
    /// assert_eq!(binfile.get_values_by_address_range(3..5),Some(vec![0x03, 0x04]));
    /// assert!(binfile.get_values_by_address_range(3..6).is_none());
    /// assert!(binfile.get_values_by_address_range(0..3).is_none());
    /// ```
    pub fn get_values_by_address_range<R>(&self, address: R) -> Option<Vec<u8>>
    where
        R: RangeBounds<usize>,
    {
        self.segments.get_values_by_address_range(&address)
    }

    /// Add given data string array by guessing its format. The format must be
    /// Motorola S-Records, Intel HEX or TI-TXT. Set `overwrite` to
    /// `true` to allow already added data to be overwritten.
    ///
    /// # Parameters
    ///
    /// - `data`: String array of records to be read in
    /// - `overwrite`: flag to activate overwrite mode
    pub fn add_strings<T, S>(&mut self, data: T, overwrite: bool) -> Result<(), Error>
    where
        T: AsRef<[S]>,
        S: AsRef<str>,
    {
        if let Some(first_line) = data.as_ref().first() {
            let data = data.as_ref();
            if SRecord::is_record_str_correct(first_line) {
                self.add_srec(data, overwrite)
            } else if IHexRecord::is_record_str_correct(first_line) {
                self.add_ihex(data, overwrite)
            } else if ExtTekHexRecord::is_record_str_correct(first_line) {
                self.add_ext_tek_hex(data, overwrite)
            } else if is_ti_txt(data) {
                self.add_ti_txt(data, overwrite)
            } else if is_verilog_vmem(data) {
                self.add_verilog_vmem(data, overwrite)
            } else {
                Err(Error::UnsupportedFileFormat)
            }
        } else {
            Ok(())
        }
    }

    /// Add given Motorola S-Records string array. Set `overwrite` to `true` to
    /// allow already added data to be overwritten.
    /// See [`BinFile.to_srec()`](#method.to_srec) for examples.
    ///
    /// # Parameters
    ///
    /// - `data`: String array of records to be read in
    /// - `overwrite`: flag to activate overwrite mode
    ///
    /// # Errors
    ///
    /// - `Error::AddDataError`: If already present datas shall be overwritten, but `overwrite` was not set
    pub fn add_srec<T, S>(&mut self, records: T, overwrite: bool) -> Result<(), Error>
    where
        T: AsRef<[S]>,
        S: AsRef<str>,
    {
        for record_str in records.as_ref() {
            match SRecord::from_record_string(record_str) {
                Ok(record) => match record {
                    SRecord::Address16(address) => {
                        self.execution_start_address = Some(address.0 as usize)
                    }
                    SRecord::Header(header) => self.header = Some(header),
                    SRecord::Data16(data) => {
                        let address = data.address.0 as usize;
                        self.segments
                            .add_segment(Segment::new(address, data.data), overwrite)?
                    }
                    SRecord::Data24(data) => {
                        let address = data.address.0 as usize;
                        self.segments
                            .add_segment(Segment::new(address, data.data), overwrite)?
                    }
                    SRecord::Data32(data) => {
                        let address = data.address.0 as usize;
                        self.segments
                            .add_segment(Segment::new(address, data.data), overwrite)?
                    }
                    SRecord::Count16(_) => (),
                    SRecord::Count24(_) => (),
                    SRecord::Address32(address) => {
                        self.execution_start_address = Some(address.0 as usize)
                    }
                    SRecord::Address24(address) => {
                        self.execution_start_address = Some(address.0 as usize)
                    }
                },
                Err(err) => return Err(err),
            }
        }
        Ok(())
    }

    /// Add given Intel HEX records string array. Set `overwrite` to `true` to
    /// allow already added data to be overwritten.
    /// See [`BinFile.to_ihex()`](#method.to_ihex) for examples.
    ///
    /// # Parameters
    ///
    /// - `data`: String array of records to be read in
    /// - `overwrite`: flag to activate overwrite mode
    ///
    /// # Errors
    ///
    /// - `Error::AddDataError`: If already present datas shall be overwritten, but `overwrite` was not set
    pub fn add_ihex<T, S>(&mut self, data: T, overwrite: bool) -> Result<(), Error>
    where
        T: AsRef<[S]>,
        S: AsRef<str>,
    {
        let mut extended_segment_address = 0;
        let mut extended_linear_address = 0;
        for record_str in data.as_ref() {
            match IHexRecord::from_record_string(record_str) {
                Ok(record) => match record {
                    IHexRecord::Data { offset, value } => {
                        let address =
                            offset as usize + extended_segment_address + extended_linear_address;
                        self.segments
                            .add_segment(Segment::new(address, value), overwrite)?;
                    }
                    IHexRecord::EndOfFile => break,
                    IHexRecord::ExtendedSegmentAddress(address) => {
                        extended_segment_address = address as usize * 16
                    }
                    IHexRecord::StartSegmentAddress(address) => {
                        self.execution_start_address = Some(address as usize)
                    }
                    IHexRecord::ExtendedLinearAddress(address) => {
                        extended_linear_address = (address as usize) << 16
                    }
                    IHexRecord::StartLinearAddress(address) => {
                        self.execution_start_address = Some(address as usize)
                    }
                },
                Err(err) => return Err(err),
            }
        }
        Ok(())
    }

    /// Add given Extended Tektronix Hex records string array. Set `overwrite` to `true` to
    /// allow already added data to be overwritten.
    /// See [`BinFile.to_ext_tek_hex()`](#method.to_ext_tek_hex) for examples.
    ///
    /// # Parameters
    ///
    /// - `data`: String array of records to be read in
    /// - `overwrite`: flag to activate overwrite mode
    ///
    /// # Errors
    ///
    /// - `Error::AddDataError`: If already present datas shall be overwritten, but `overwrite` was not set
    pub fn add_ext_tek_hex<T, S>(&mut self, data: T, overwrite: bool) -> Result<(), Error>
    where
        T: AsRef<[S]>,
        S: AsRef<str>,
    {
        for record_str in data.as_ref() {
            match ExtTekHexRecord::from_record_string(record_str)? {
                ExtTekHexRecord::Data { address, value } => {
                    self.segments
                        .add_segment(Segment::new(address, value), overwrite)?;
                }
                ExtTekHexRecord::Termination { start_address } => {
                    self.execution_start_address = Some(start_address);
                    break;
                }
                ExtTekHexRecord::Symbol(_) => (),
            }
        }
        Ok(())
    }

    /// Add given TI-TXT string `lines` as array. Set `overwrite` to `true` to
    /// allow already added data to be overwritten.
    /// See [`BinFile.to_ti_tex()`](#method.to_ti_tex) for examples.
    ///
    /// # Parameters
    ///
    /// - `data`: String array of records to be read in
    /// - `overwrite`: flag to activate overwrite mode
    ///
    /// # Errors
    ///
    /// - `Error::AddDataError`: If already present datas shall be overwritten, but `overwrite` was not set
    pub fn add_ti_txt<T, S>(&mut self, data: T, overwrite: bool) -> Result<(), Error>
    where
        T: AsRef<[S]>,
        S: AsRef<str>,
    {
        let mut address = None;
        let mut eof_found = false;

        for line in data.as_ref() {
            if eof_found {
                return Err(Error::UnsupportedFileFormat);
            }
            let line = line.as_ref().trim();

            if line.is_empty() {
                return Err(Error::RecordTooShort);
            }
            if line.starts_with('q') {
                eof_found = true;
            } else if line.starts_with('@') {
                if let Ok(add) = u32::from_str_radix(line.trim_start_matches('@'), 16) {
                    address = Some(add as usize);
                } else {
                    return Err(Error::ContainsInvalidCharacters);
                }
            } else {
                let mut data_bytes = Vec::new();
                // Convert the character stream to bytes.
                for byte_str in line
                    .replace(' ', "")
                    .as_bytes()
                    .chunks(2)
                    .map(|chunk| std::str::from_utf8(chunk).unwrap())
                {
                    if let Ok(value) = u8::from_str_radix(byte_str, 16) {
                        data_bytes.push(value);
                    } else {
                        return Err(Error::ContainsInvalidCharacters);
                    }
                }
                let size = data_bytes.len();

                if size > TI_TXT_BYTES_PER_LINE {
                    return Err(Error::RecordTooLong);
                }
                if let Some(addr) = address {
                    self.segments
                        .add_segment(Segment::new(addr, data_bytes), overwrite)?;
                    if size == TI_TXT_BYTES_PER_LINE {
                        address = Some(addr + size);
                    } else {
                        address = None;
                    }
                } else {
                    return Err(Error::MissingStartCode);
                }
            }
        }
        if !eof_found {
            return Err(Error::UnsupportedFileFormat);
        }
        Ok(())
    }

    /// Add given Verlog-Vmem string `lines` array. Set `overwrite` to `true` to
    /// allow already added data to be overwritten.
    /// See [`BinFile.to_verilog_vmem()`](#method.to_verilog_vmem) for examples.
    ///
    /// # Parameters
    ///
    /// - `data`: String array of records to be read in
    /// - `overwrite`: flag to activate overwrite mode
    ///
    /// # Errors
    ///
    /// - `Error::AddDataError`: If already present datas shall be overwritten, but `overwrite` was not set
    pub fn add_verilog_vmem<T, S>(&mut self, data: T, overwrite: bool) -> Result<(), Error>
    where
        T: AsRef<[S]>,
        S: AsRef<str>,
    {
        let mut address = None;
        let regex = Regex::new(r"\s+").unwrap();
        let mut chunk = Vec::new();
        let data: Vec<&str> = data.as_ref().iter().map(|val| val.as_ref()).collect();
        let data = data.join("\n\r");
        let comment_removed_string = comment_remover(&data);
        let words = regex.split(comment_removed_string.trim());
        let mut word_size_bytes = None;
        let words: Vec<&str> = words.into_iter().collect();

        for word in &words {
            if !word.starts_with('@') {
                let mut length = word.len();

                if length % 2 != 0 {
                    return Err(Error::UnsupportedFileFormat);
                }
                length /= 2;
                if let Some(wsl) = word_size_bytes {
                    if wsl != length {
                        return Err(Error::UnsupportedFileFormat);
                    }
                } else {
                    word_size_bytes = Some(length);
                }
            }
        }
        for word in words {
            if word.starts_with('@') {
                if let Some(addr) = address {
                    self.segments
                        .add_segment(Segment::new(addr, chunk), overwrite)?;
                }
                address = usize::from_str_radix(word.trim_start_matches('@'), 16).ok();
                chunk = Vec::new();
            } else {
                let mut data_bytes = word
                    .replace(' ', "")
                    .as_bytes()
                    .chunks(2)
                    .map(|chunk| std::str::from_utf8(chunk).unwrap())
                    .map(|byte_str| u8::from_str_radix(byte_str, 16).unwrap())
                    .collect::<Vec<u8>>();
                chunk.append(&mut data_bytes);
            }
        }
        if let Some(addr) = address {
            if !chunk.is_empty() {
                self.segments
                    .add_segment(Segment::new(addr, chunk), overwrite)?;
            }
        }
        Ok(())
    }

    /// Add given data at given address. Set `overwrite` to `true` to
    /// allow already added data to be overwritten.
    /// See [`BinFile.to_bytes()`](#method.to_bytes) for examples.
    ///
    /// # Parameters
    ///
    /// - `data`: String array of records to be read in
    /// - `address`: Address were the data shall be inserted
    /// - `overwrite`: flag to activate overwrite mode
    ///
    /// # Errors
    ///
    /// - `Error::AddDataError`: If already present datas shall be overwritten, but `overwrite` was not set
    pub fn add_bytes<T>(
        &mut self,
        data: T,
        address: Option<usize>,
        overwrite: bool,
    ) -> Result<(), Error>
    where
        T: AsRef<[u8]>,
    {
        let address = address.unwrap_or(0);
        self.segments
            .add_segment(Segment::new(address, data), overwrite)
    }

    /// Open given file and add its data by guessing its format. The format
    /// must be Motorola S-Records, Intel HEX, TI-TXT. Set `overwrite`
    /// to `true` to allow already added data to be overwritten.
    ///
    /// # Parameters
    ///
    /// - `file_name`: File name of the file
    /// - `overwrite`: flag to activate overwrite mode
    ///
    /// # Errors
    ///
    /// - `Error::AddDataError`: If already present datas shall be overwritten, but `overwrite` was not set
    /// - `Error::IoError`: Any other error occurred while loading the input data
    pub fn add_file<P>(&mut self, file_name: P, overwrite: bool) -> Result<(), Error>
    where
        P: AsRef<Path>,
    {
        self.add_strings(self._open_input(file_name)?.as_slice(), overwrite)?;
        Ok(())
    }

    /// Open given Motorola S-Records file and add its records. Set
    /// `overwrite` to `true` to allow already added data to be
    /// overwritten.
    ///
    /// # Parameters
    ///
    /// - `file_name`: File name of the file
    /// - `overwrite`: flag to activate overwrite mode
    ///
    /// # Errors
    ///
    /// - `Error::AddDataError`: If already present datas shall be overwritten, but `overwrite` was not set
    /// - `Error::IoError`: Any other error occurred while loading the input data
    pub fn add_srec_file<P>(&mut self, file_name: P, overwrite: bool) -> Result<(), Error>
    where
        P: AsRef<Path>,
    {
        self.add_srec(self._open_input(file_name)?.as_slice(), overwrite)?;
        Ok(())
    }

    /// Open given Intel HEX file and add its records. Set `overwrite` to
    /// `true` to allow already added data to be overwritten.
    ///
    /// # Parameters
    ///
    /// - `file_name`: File name of the file
    /// - `overwrite`: flag to activate overwrite mode
    ///
    /// # Errors
    ///
    /// - `Error::AddDataError`: If already present datas shall be overwritten, but `overwrite` was not set
    /// - `Error::IoError`: Any other error occurred while loading the input data
    pub fn add_ihex_file<P>(&mut self, file_name: P, overwrite: bool) -> Result<(), Error>
    where
        P: AsRef<Path>,
    {
        self.add_ihex(self._open_input(file_name)?.as_slice(), overwrite)?;
        Ok(())
    }

    /// Open given Extended Tektronix Hex file and add its records. Set `overwrite` to
    /// `true` to allow already added data to be overwritten.
    ///
    /// # Parameters
    ///
    /// - `file_name`: File name of the file
    /// - `overwrite`: flag to activate overwrite mode
    ///
    /// # Errors
    ///
    /// - `Error::AddDataError`: If already present datas shall be overwritten, but `overwrite` was not set
    /// - `Error::IoError`: Any other error occurred while loading the input data
    pub fn add_ext_tek_hex_file<P>(&mut self, file_name: P, overwrite: bool) -> Result<(), Error>
    where
        P: AsRef<Path>,
    {
        self.add_ext_tek_hex(self._open_input(file_name)?.as_slice(), overwrite)?;
        Ok(())
    }

    /// Open given TI-TXT file and add its contents. Set `overwrite` to
    /// `true` to allow already added data to be overwritten.
    ///
    /// # Parameters
    ///
    /// - `file_name`: File name of the file
    /// - `overwrite`: flag to activate overwrite mode
    ///
    /// # Errors
    ///
    /// - `Error::AddDataError`: If already present datas shall be overwritten, but `overwrite` was not set
    /// - `Error::IoError`: Any other error occurred while loading the input data
    pub fn add_ti_txt_file<P>(&mut self, file_name: P, overwrite: bool) -> Result<(), Error>
    where
        P: AsRef<Path>,
    {
        self.add_ti_txt(self._open_input(file_name)?.as_slice(), overwrite)?;
        Ok(())
    }

    /// Open given Verilog VMEM file and add its contents. Set `overwrite` to
    /// `true` to allow already added data to be overwritten.
    ///
    /// # Parameters
    ///
    /// - `file_name`: File name of the file
    /// - `overwrite`: flag to activate overwrite mode
    ///
    /// # Errors
    ///
    /// - `Error::AddDataError`: If already present datas shall be overwritten, but `overwrite` was not set
    /// - `Error::IoError`: Any other error occurred while loading the input data
    pub fn add_verilog_vmem_file<P>(&mut self, file_name: P, overwrite: bool) -> Result<(), Error>
    where
        P: AsRef<Path>,
    {
        self.add_verilog_vmem(self._open_input(file_name)?.as_slice(), overwrite)?;
        Ok(())
    }

    /// Private method which reads in a given text file and returns the content as `Vec<String>`
    ///
    /// # Parameters
    ///
    /// - `file_name`: File name of the file
    ///
    /// # Errors
    ///
    /// - `Error::IoError`: Error while reading the file
    fn _open_input<P>(&self, file_name: P) -> Result<Vec<String>, Error>
    where
        P: AsRef<Path>,
    {
        if let Ok(file) = File::open(file_name) {
            let reader = BufReader::new(file);
            let data: Vec<String> = reader.lines().map_while(Result::ok).collect();
            Ok(data)
        } else {
            Err(Error::IoError)
        }
    }

    /// Open given binary file and add its contents add the given address.
    /// Set `overwrite` to `true` to allow already added data to be overwritten.
    ///
    /// # Parameters
    ///
    /// - `file_name`: File name of the file
    /// - `address`: Address where datas will be written
    /// - `overwrite`: flag to activate overwrite mode
    ///
    /// # Errors
    ///
    /// - `Error::AddDataError`: If already present datas shall be overwritten, but `overwrite` was not set
    /// - `Error::IoError`: Any other error occurred while loading the input data
    pub fn add_binary_file<P>(
        &mut self,
        file_name: P,
        address: Option<usize>,
        overwrite: bool,
    ) -> Result<(), Error>
    where
        P: AsRef<Path>,
    {
        if let Ok(mut file) = File::open(&file_name) {
            let metadata = metadata(&file_name).unwrap();
            let mut data: Vec<u8> = vec![0; metadata.len() as usize];
            if let Ok(n) = file.read(&mut data) {
                self.add_bytes(&data[..n], address, overwrite)?;
            } else {
                return Err(Error::IoError);
            }
            return Ok(());
        }
        Err(Error::IoError)
    }

    fn to_srec_int(
        &self,
        number_of_data_bytes: Option<usize>,
        address_length_bits: SRecordAddressLength,
        pretty: bool,
    ) -> Result<Vec<String>, Error> {
        if let Some(maximum_address) = self.maximum_address() {
            match address_length_bits {
                SRecordAddressLength::Length16 => {
                    if maximum_address > 0xFFFF {
                        return Err(Error::AddressTooBig);
                    }
                }
                SRecordAddressLength::Length24 => {
                    if maximum_address > 0xFFFFFF {
                        return Err(Error::AddressTooBig);
                    }
                }
                SRecordAddressLength::Length32 => {
                    if maximum_address > 0xFFFFFFFF {
                        return Err(Error::AddressTooBig);
                    }
                }
            }
        }
        let number_of_data_bytes = number_of_data_bytes.unwrap_or(32);
        let mut lines = Vec::new();
        if let Some(header) = &self.header {
            let record = SRecord::Header(header.to_owned());
            if pretty {
                lines.push(record.to_pretty_record_string()?)
            } else {
                lines.push(record.to_record_string()?)
            }
        }
        let mut number_of_records = 0;
        let data = self.segments.chunks(Some(number_of_data_bytes), None)?;
        for (address, datas) in data {
            let record = match address_length_bits {
                SRecordAddressLength::Length16 => SRecord::Data16(Data {
                    address: Address16(address as u16),
                    data: datas,
                }),
                SRecordAddressLength::Length24 => SRecord::Data24(Data {
                    address: Address24(address as u32),
                    data: datas,
                }),
                SRecordAddressLength::Length32 => SRecord::Data32(Data {
                    address: Address32(address as u32),
                    data: datas,
                }),
            };
            if pretty {
                lines.push(record.to_pretty_record_string()?)
            } else {
                lines.push(record.to_record_string()?)
            }
            number_of_records += 1;
        }
        let record = if number_of_records <= 0xFFFF {
            SRecord::Count16(Count16(number_of_records as u16))
        } else {
            SRecord::Count24(Count24(number_of_records))
        };
        if pretty {
            lines.push(record.to_pretty_record_string()?)
        } else {
            lines.push(record.to_record_string()?)
        }

        // Add the execution start address
        if let Some(execution_start_address) = self.execution_start_address {
            let record = match address_length_bits {
                SRecordAddressLength::Length16 => {
                    SRecord::Address16(Address16(execution_start_address as u16))
                }
                SRecordAddressLength::Length24 => {
                    SRecord::Address24(Address24(execution_start_address as u32))
                }
                SRecordAddressLength::Length32 => {
                    SRecord::Address32(Address32(execution_start_address as u32))
                }
            };
            if pretty {
                lines.push(record.to_pretty_record_string()?)
            } else {
                lines.push(record.to_record_string()?)
            }
        }
        Ok(lines)
    }

    /// Format the binary file as Motorola S-Records records and return
    /// them as a string.
    ///
    /// # Parameters
    ///
    /// - `number_of_data_bytes` is the number of data bytes in each
    ///   record.
    /// - `address_length_bits` is the number of address bits in each
    ///   record. If you're not sure what to take use `SRecordAddressLength::default()`
    ///
    /// # Errors
    ///
    /// - `Error::AlignmentToSizeError`: Should not occure, only if someone chaged alignment default value
    /// - `Error::AddressTooBig`: If maximum address doesn't fit in the selected file format. Choose bigger
    ///   `address_length_bits` if possible
    ///
    /// # Examples
    ///
    /// ```rust
    /// use bin_file::BinFile;
    /// use bin_file::SRecordAddressLength;
    /// use bin_file::Error;
    ///
    /// fn main() -> Result<(), Error> {
    ///     let mut binfile = BinFile::new();
    ///     assert!(binfile.add_srec(
    ///         [
    ///             "S32500000100214601360121470136007EFE09D219012146017E17C20001FF5F16002148011973",
    ///             "S32500000120194E79234623965778239EDA3F01B2CA3F0156702B5E712B722B73214601342199",
    ///             "S5030002FA",
    ///         ], false).is_ok());
    ///     print!("{}", binfile.to_srec(None, SRecordAddressLength::default())?.join("\n\r"));
    ///     Ok(())
    /// }
    /// ```
    ///
    /// ```console
    /// S32500000100214601360121470136007EFE09D219012146017E17C20001FF5F16002148011973
    /// S32500000120194E79234623965778239EDA3F01B2CA3F0156702B5E712B722B73214601342199
    /// S5030002FA
    /// ```
    pub fn to_srec(
        &self,
        number_of_data_bytes: Option<usize>,
        address_length_bits: SRecordAddressLength,
    ) -> Result<Vec<String>, Error> {
        self.to_srec_int(number_of_data_bytes, address_length_bits, false)
    }

    /// Format the binary file as Motorola S-Records records and return
    /// them as a string with added coloring and comments
    ///
    /// Important: File is not readable afterwards
    ///
    /// # Parameters
    ///
    /// - `number_of_data_bytes` is the number of data bytes in each
    ///   record.
    /// - `address_length_bits` is the number of address bits in each
    ///   record. If you're not sure what to take use `SRecordAddressLength::default()`
    ///
    /// # Errors
    ///
    /// - `Error::AlignmentToSizeError`: Should not occure, only if someone chaged alignment default value
    /// - `Error::AddressTooBig`: If maximum address doesn't fit in the selected file format. Choose bigger
    ///   `address_length_bits` if possible
    ///
    /// # Examples
    ///
    /// ```rust
    /// use bin_file::BinFile;
    /// use bin_file::SRecordAddressLength;
    /// use bin_file::Error;
    ///
    /// fn main() -> Result<(), Error> {
    ///     let mut binfile = BinFile::new();
    ///     assert!(binfile.add_srec(
    ///         [
    ///             "S32500000100214601360121470136007EFE09D219012146017E17C20001FF5F16002148011973",
    ///             "S32500000120194E79234623965778239EDA3F01B2CA3F0156702B5E712B722B73214601342199",
    ///             "S5030002FA",
    ///         ], false).is_ok());
    ///     print!("{}", binfile.to_srec_pretty(None, SRecordAddressLength::default())?.join("\n\r"));
    ///     Ok(())
    /// }
    /// ```
    pub fn to_srec_pretty(
        &self,
        number_of_data_bytes: Option<usize>,
        address_length_bits: SRecordAddressLength,
    ) -> Result<Vec<String>, Error> {
        self.to_srec_int(number_of_data_bytes, address_length_bits, true)
    }

    fn to_ihex_int(
        &self,
        number_of_bytes: Option<usize>,
        ihex_format: IHexFormat,
        pretty: bool,
    ) -> Result<Vec<String>, Error> {
        let i32hex = |address: usize,
                      extended_linear_address: u32,
                      lines: &mut Vec<String>|
         -> Result<(usize, u32), Error> {
            if address > 0xffffffff {
                return Err(Error::AddressTooBig);
            }
            let mut extended_linear_address = extended_linear_address;
            let address_upper_16_bits = (address >> 16) & 0xffff;
            if address_upper_16_bits > extended_linear_address as usize {
                extended_linear_address = address_upper_16_bits as u32;
                let record = IHexRecord::ExtendedLinearAddress(address_upper_16_bits as u16);
                lines.push(record.to_record_string().unwrap());
            }
            Ok((address, extended_linear_address))
        };
        let i16hex = |address: usize,
                      extended_segment_address: u32,
                      lines: &mut Vec<String>|
         -> Result<(usize, u32), Error> {
            if address > 16 * 0xffff + 0xffff {
                return Err(Error::AddressTooBig);
            }
            let mut extended_segment_address = extended_segment_address;
            let mut address_lower = address - 16 * extended_segment_address as usize;

            if address_lower > 0xffff {
                extended_segment_address = 4096 * (address >> 16) as u32;
                if extended_segment_address > 0xffff {
                    extended_segment_address = 0xffff;
                }
                address_lower = address - 16 * extended_segment_address as usize;
                let record = IHexRecord::ExtendedSegmentAddress(extended_segment_address as u16);
                lines.push(record.to_record_string().unwrap());
            }
            Ok((address_lower, extended_segment_address))
        };
        let i8hex = |address: usize| -> Result<(), Error> {
            if address > 0xffff {
                return Err(Error::AddressTooBig);
            }
            Ok(())
        };

        let mut lines = Vec::new();
        let mut extended_segment_address = 0;
        let mut extended_linear_address = 0;
        let number_of_data_words = number_of_bytes.unwrap_or(32);

        for (address, data) in self.segments.chunks(Some(number_of_data_words), None)? {
            let mut address = address;
            match ihex_format {
                IHexFormat::IHex8 => i8hex(address)?,
                IHexFormat::IHex16 => {
                    (address, extended_segment_address) =
                        i16hex(address, extended_segment_address, &mut lines)?
                }
                IHexFormat::IHex32 => {
                    (address, extended_linear_address) =
                        i32hex(address, extended_linear_address, &mut lines)?
                }
            }
            let record = IHexRecord::Data {
                offset: address as u16,
                value: data,
            };
            if pretty {
                lines.push(record.to_pretty_record_string()?)
            } else {
                lines.push(record.to_record_string()?)
            }
        }
        if let Some(start_address) = self.execution_start_address {
            match ihex_format {
                IHexFormat::IHex8 => (),
                IHexFormat::IHex16 => {
                    let record = IHexRecord::StartSegmentAddress(start_address as u32);
                    if pretty {
                        lines.push(record.to_pretty_record_string()?)
                    } else {
                        lines.push(record.to_record_string()?)
                    }
                }
                IHexFormat::IHex32 => {
                    let record = IHexRecord::StartLinearAddress(start_address as u32);
                    if pretty {
                        lines.push(record.to_pretty_record_string()?)
                    } else {
                        lines.push(record.to_record_string()?)
                    }
                }
            }
        }
        if pretty {
            lines.push(IHexRecord::EndOfFile.to_pretty_record_string()?)
        } else {
            lines.push(IHexRecord::EndOfFile.to_record_string()?)
        }
        Ok(lines)
    }

    /// Format the binary file as Intel HEX records and return them as a
    /// string.
    ///
    /// # Parameters
    /// - `number_of_data_bytes` is the number of data bytes in each
    ///   record.
    /// - `ihex_format` is the number of address bits in each
    ///   record. If you're not sure what to take use `IHexFormat::default()`
    ///
    /// # Examples
    ///
    /// ```rust
    /// use bin_file::BinFile;
    /// use bin_file::IHexFormat;
    /// use bin_file::Error;
    ///
    /// fn main() -> Result<(), Error> {
    ///     let mut binfile = BinFile::new();
    ///
    ///     assert!(binfile.add_ihex(
    ///         [
    ///         ":20010000214601360121470136007EFE09D219012146017E17C20001FF5F16002148011979",
    ///         ":20012000194E79234623965778239EDA3F01B2CA3F0156702B5E712B722B7321460134219F",
    ///         ":00000001FF"
    ///         ], false).is_ok());
    ///     print!("{}", binfile.to_ihex(None, IHexFormat::default())?.join("\n\r"));
    ///     Ok(())
    /// }
    /// ```
    ///
    /// ```console
    /// :20010000214601360121470136007EFE09D219012146017E17C20001FF5F16002148011979
    /// :20012000194E79234623965778239EDA3F01B2CA3F0156702B5E712B722B7321460134219F
    /// :00000001FF
    /// ```
    pub fn to_ihex(
        &self,
        number_of_bytes: Option<usize>,
        ihex_format: IHexFormat,
    ) -> Result<Vec<String>, Error> {
        self.to_ihex_int(number_of_bytes, ihex_format, false)
    }

    /// Format the binary file as Intel HEX records and return them as a
    /// string with added coloring and comments
    ///
    /// Important: File is not readable afterwards
    ///
    /// # Parameters
    /// - `number_of_data_bytes` is the number of data bytes in each
    ///   record.
    /// - `ihex_format` is the number of address bits in each
    ///   record. If you're not sure what to take use `IHexFormat::default()`
    ///
    /// # Examples
    ///
    /// ```rust
    /// use bin_file::BinFile;
    /// use bin_file::IHexFormat;
    /// use bin_file::Error;
    ///
    /// fn main() -> Result<(), Error> {
    ///     let mut binfile = BinFile::new();
    ///
    ///     assert!(binfile.add_ihex(
    ///         [
    ///         ":20010000214601360121470136007EFE09D219012146017E17C20001FF5F16002148011979",
    ///         ":20012000194E79234623965778239EDA3F01B2CA3F0156702B5E712B722B7321460134219F",
    ///         ":00000001FF"
    ///         ], false).is_ok());
    ///     print!("{}", binfile.to_ihex_pretty(None, IHexFormat::default())?.join("\n\r"));
    ///     Ok(())
    /// }
    /// ```
    pub fn to_ihex_pretty(
        &self,
        number_of_bytes: Option<usize>,
        ihex_format: IHexFormat,
    ) -> Result<Vec<String>, Error> {
        self.to_ihex_int(number_of_bytes, ihex_format, true)
    }

    fn to_ext_tek_hex_int(
        &self,
        number_of_bytes_per_line: Option<u8>,
        pretty: bool,
    ) -> Result<Vec<String>, Error> {
        let mut lines = Vec::new();
        let number_of_data_words =
            number_of_bytes_per_line.unwrap_or(EXT_TEK_HEX_BYTES_PER_LINE) as usize;
        if number_of_data_words > 255 - 6 {
            Err(Error::RecordTooLong)
        } else {
            for segment in self.segments.segments() {
                for (address, data) in segment.chunks(Some(number_of_data_words), None)? {
                    let record = ExtTekHexRecord::Data {
                        address,
                        value: data,
                    };
                    if pretty {
                        lines.push(record.to_pretty_record_string()?);
                    } else {
                        lines.push(record.to_record_string()?);
                    }
                }
            }
            Ok(lines)
        }
    }

    /// Format the binary file as a Extended Tektronix Hex
    /// file and return it as a string.
    ///
    /// # Parameters
    ///
    /// - `number_of_bytes_per_line`: Number of bytes printed per line.
    ///   If set to None 16 Bytes will be printed
    ///
    /// # Examples
    ///
    /// ```rust
    /// use bin_file::BinFile;
    /// use bin_file::Error;
    ///
    /// fn main() -> Result<(), Error> {
    ///     let mut binfile = BinFile::new();
    ///
    ///     assert!(binfile.add_ext_tek_hex(
    ///         ["%1A626810000000202020202020"], false).is_ok());
    ///     print!("{}", binfile.to_ext_tek_hex(None)?.join("\n\r"));
    ///     Ok(())
    /// }
    /// ```
    ///
    /// ```console
    /// %1A626810000000202020202020
    /// ```
    pub fn to_ext_tek_hex(
        &self,
        number_of_bytes_per_line: Option<u8>,
    ) -> Result<Vec<String>, Error> {
        self.to_ext_tek_hex_int(number_of_bytes_per_line, false)
    }

    /// Format the binary file as a Extended Tektronix Hex
    /// file and return it as a string with added coloring and comments
    ///
    /// Important: File is not readable afterwards
    ///
    /// # Parameters
    ///
    /// - `number_of_bytes_per_line`: Number of bytes printed per line.
    ///   If set to None 16 Bytes will be printed
    ///
    /// # Examples
    ///
    /// ```rust
    /// use bin_file::BinFile;
    /// use bin_file::Error;
    ///
    /// fn main() -> Result<(), Error> {
    ///     let mut binfile = BinFile::new();
    ///
    ///     assert!(binfile.add_ext_tek_hex(
    ///         ["%1A626810000000202020202020"], false).is_ok());
    ///     print!("{}", binfile.to_ext_tek_hex_pretty(None)?.join("\n\r"));
    ///     Ok(())
    /// }
    pub fn to_ext_tek_hex_pretty(
        &self,
        number_of_bytes_per_line: Option<u8>,
    ) -> Result<Vec<String>, Error> {
        self.to_ext_tek_hex_int(number_of_bytes_per_line, true)
    }

    fn to_ti_txt_int(&self, pretty: bool) -> Result<Vec<String>, Error> {
        let mut lines = Vec::new();
        let number_of_data_words = TI_TXT_BYTES_PER_LINE;

        for segment in self.segments.segments() {
            lines.push(if pretty {
                format!(
                    "{} (segment address)",
                    ansi_term::Colour::Yellow.paint(format!("@{:04X}", segment.minimum_address()))
                )
            } else {
                format!("@{:04X}", segment.minimum_address())
            });
            for (_, data) in segment.chunks(Some(number_of_data_words), None)? {
                let mut string = String::new();
                for value in data {
                    string += format!("{:02X} ", value).as_str();
                }
                lines.push(if pretty {
                    format!("{} (data)", string.trim_end())
                } else {
                    string.trim_end().to_string()
                });
            }
        }
        if pretty {
            lines.push(format!(
                "{} (end of file)",
                ansi_term::Colour::Fixed(35).paint("q"),
            ))
        } else {
            lines.push("q".into());
        }
        Ok(lines)
    }

    /// Format the binary file as a TI-TXT file and return it as a string array.
    ///
    /// # Examples
    ///
    /// ```rust
    /// use bin_file::BinFile;
    /// use bin_file::Error;
    ///
    /// fn main() -> Result<(), Error> {
    ///     let mut binfile = BinFile::new();
    ///
    ///     assert!(binfile.add_ti_txt(
    ///         [
    ///         "@0100",
    ///         "21 46 01 36 01 21 47 01 36 00 7E FE 09 D2 19 01",
    ///         "21 46 01 7E 17 C2 00 01 FF 5F 16 00 21 48 01 19",
    ///         "19 4E 79 23 46 23 96 57 78 23 9E DA 3F 01 B2 CA",
    ///         "3F 01 56 70 2B 5E 71 2B 72 2B 73 21 46 01 34 21",
    ///         "q",
    ///         ], false).is_ok());
    ///     print!("{}", binfile.to_ti_txt()?.join("\n\r"));
    ///     Ok(())
    /// }
    /// ```
    ///
    /// ```console
    /// @0100
    /// 21 46 01 36 01 21 47 01 36 00 7E FE 09 D2 19 01
    /// 21 46 01 7E 17 C2 00 01 FF 5F 16 00 21 48 01 19
    /// 19 4E 79 23 46 23 96 57 78 23 9E DA 3F 01 B2 CA
    /// 3F 01 56 70 2B 5E 71 2B 72 2B 73 21 46 01 34 21
    /// q
    /// ```
    pub fn to_ti_txt(&self) -> Result<Vec<String>, Error> {
        self.to_ti_txt_int(false)
    }

    /// Format the binary file as a TI-TXT file and return it as a string array
    ///  with added coloring and comments
    ///
    /// Important: File is not readable afterwards
    ///
    /// # Examples
    ///
    /// ```rust
    /// use bin_file::BinFile;
    /// use bin_file::Error;
    ///
    /// fn main() -> Result<(), Error> {
    ///     let mut binfile = BinFile::new();
    ///
    ///     assert!(binfile.add_ti_txt(
    ///         [
    ///         "@0100",
    ///         "21 46 01 36 01 21 47 01 36 00 7E FE 09 D2 19 01",
    ///         "21 46 01 7E 17 C2 00 01 FF 5F 16 00 21 48 01 19",
    ///         "19 4E 79 23 46 23 96 57 78 23 9E DA 3F 01 B2 CA",
    ///         "3F 01 56 70 2B 5E 71 2B 72 2B 73 21 46 01 34 21",
    ///         "q",
    ///         ], false).is_ok());
    ///     print!("{}", binfile.to_ti_txt_pretty()?.join("\n\r"));
    ///     Ok(())
    /// }
    /// ```
    ///
    /// ```console
    /// \033[0;33m@0100\033[0m (segment address)
    /// 21 46 01 36 01 21 47 01 36 00 7E FE 09 D2 19 01 (data)
    /// 21 46 01 7E 17 C2 00 01 FF 5F 16 00 21 48 01 19 (data)
    /// 19 4E 79 23 46 23 96 57 78 23 9E DA 3F 01 B2 CA (data)
    /// 3F 01 56 70 2B 5E 71 2B 72 2B 73 21 46 01 34 21 (data)
    /// 033[0;35mq\033[0m (end of file)
    /// ```
    pub fn to_ti_txt_pretty(&self) -> Result<Vec<String>, Error> {
        self.to_ti_txt_int(true)
    }

    /// Format the binary file as a Verilog VMEM file and return it as a string.
    ///
    /// # Examples
    ///
    /// ```rust
    /// use bin_file::BinFile;
    /// use bin_file::Error;
    ///
    /// fn main() -> Result<(), Error> {
    ///     let mut binfile = BinFile::new();
    ///
    ///     assert!(binfile.add_verilog_vmem(
    ///         [
    ///         "@00000100 21 46 01 36 01 21 47 01 36 00 7E FE 09 D2 19 01 21 46 01 7E 17 C2 00 01 FF 5F 16 00 21 48 01 19",
    ///         "@00000120 19 4E 79 23 46 23 96 57 78 23 9E DA 3F 01 B2 CA 3F 01 56 70 2B 5E 71 2B 72 2B 73 21 46 01 34 21",
    ///         ], false).is_ok());
    ///     print!("{}", binfile.to_verilog_vmem()?.join("\n\r"));
    ///     Ok(())
    /// }
    /// ```
    ///
    /// ```console
    /// @00000100 21 46 01 36 01 21 47 01 36 00 7E FE 09 D2 19 01 21 46 01 7E 17 C2 00 01 FF 5F 16 00 21 48 01 19
    /// @00000120 19 4E 79 23 46 23 96 57 78 23 9E DA 3F 01 B2 CA 3F 01 56 70 2B 5E 71 2B 72 2B 73 21 46 01 34 21
    /// ```
    pub fn to_verilog_vmem(&self) -> Result<Vec<String>, Error> {
        let mut lines = Vec::new();
        if let Some(header) = self.header.clone() {
            lines.push(format!("/* {} */", String::from_utf8(header).unwrap()));
        }
        for segment in self.segments.segments() {
            for (address, data) in segment.chunks(Some(VERILOG_VMEM_BYTES_PER_LINE), None)? {
                let mut string = format!("@{:08X}", address);
                for value in data {
                    string += format!(" {:02x}", value).as_str();
                }
            }
        }
        Ok(lines)
    }

    /// Return a `Vec<u8>` of all data within given address range.
    ///
    /// # Parameters
    ///
    /// - `range`: the address range which shall be exported
    /// - `padding`: is the word value of the padding between
    ///   non-adjacent segments. If not provided `0xFF`will be used
    ///
    /// # Examples
    ///
    /// ```rust
    /// use bin_file::BinFile;
    /// use bin_file::Error;
    ///
    /// fn main() -> Result<(), Error> {
    ///     let mut binfile = BinFile::new();
    ///     let data = [0x21, 0x46, 0x01, 0x36, 0x01, 0x21, 0x47, 0x01, 0x36, 0x00, 0x7E, 0xFE, 0x09, 0xD2, 0x19, 0x01,
    ///                 0x21, 0x46, 0x01, 0x7E, 0x17, 0xC2, 0x00, 0x01, 0xFF, 0x5F, 0x16, 0x00, 0x21, 0x48, 0x01, 0x19,
    ///                 0x19, 0x4E, 0x79, 0x23, 0x46, 0x23, 0x96, 0x57, 0x78, 0x23, 0x9E, 0xDA, 0x3F, 0x01, 0xB2, 0xCA,
    ///                 0x3F, 0x01, 0x56, 0x70, 0x2B, 0x5E, 0x71, 0x2B, 0x72, 0x2B, 0x73, 0x21, 0x46, 0x01, 0x34, 0x21];
    ///
    ///     assert!(binfile.add_bytes(data, Some(0x100), false).is_ok());
    ///     assert_eq!(binfile.to_bytes(.., None), Ok(data.to_vec()));
    ///     Ok(())
    /// }
    /// ```
    pub fn to_bytes<R>(&self, range: R, padding: Option<u8>) -> Result<Vec<u8>, Error>
    where
        R: RangeBounds<usize>,
    {
        if self.segments.segments().is_empty() {
            return Ok(Vec::new());
        }
        let mut current_maximum_address = match range.start_bound() {
            std::ops::Bound::Included(val) => *val,
            std::ops::Bound::Excluded(val) => *val + 1,
            std::ops::Bound::Unbounded => self.minimum_address().unwrap(),
        };
        let maximum_address = match range.end_bound() {
            std::ops::Bound::Included(val) => *val + 1,
            std::ops::Bound::Excluded(val) => *val,
            std::ops::Bound::Unbounded => self.maximum_address().unwrap(),
        };

        if current_maximum_address >= maximum_address {
            // Asked address is upper of data
            return Ok(Vec::new());
        }
        let padding = padding.unwrap_or(0xFF);
        let mut binary = Vec::new();

        for segment in self.segments.segments() {
            let (mut address, data) = segment.get_tuple();
            let mut data = data.to_vec();
            let mut length = data.len();

            // Discard data below the minimum
            if address < current_maximum_address {
                if address + length <= current_maximum_address {
                    continue;
                }
                let offset = current_maximum_address - address;
                data = data[offset..].to_vec();
                length = data.len();
                address = current_maximum_address;
            }

            // Discard data above the maximum address
            if address + length > maximum_address {
                if address < maximum_address {
                    let size = maximum_address - address;
                    data = data[..size].to_vec();
                } else if maximum_address > current_maximum_address {
                    let mut pad_vec = vec![padding; maximum_address - current_maximum_address];
                    binary.append(&mut pad_vec);
                }
            }
            if current_maximum_address < address {
                let mut pad_vec = vec![padding; address - current_maximum_address];
                binary.append(&mut pad_vec);
            }
            binary.append(&mut data);
            current_maximum_address = address + length;
        }
        Ok(binary)
    }

    /// Format the binary file as a string values separated by given
    /// separator `separator`. This function can be used to generate
    /// array initialization code for C and other languages.
    ///
    /// # Parameters
    ///
    /// - `range`: the address range which shall be exported
    /// - `padding`: is the word value of the padding between
    ///   non-adjacent segments. If not provided `0xFF`will be used
    /// - `seperator`: connecting string between two bytes
    ///
    /// # Examples
    ///
    /// ```rust
    /// use bin_file::BinFile;
    /// use bin_file::Error;
    ///
    /// fn main() -> Result<(), Error> {
    ///     let mut binfile = BinFile::new();
    ///     let data = [0x21, 0x46, 0x01, 0x36, 0x01, 0x21, 0x47, 0x01, 0x36, 0x00, 0x7E, 0xFE, 0x09, 0xD2, 0x19, 0x01,
    ///                 0x21, 0x46, 0x01, 0x7E, 0x17, 0xC2, 0x00, 0x01, 0xFF, 0x5F, 0x16, 0x00, 0x21, 0x48, 0x01, 0x19,
    ///                 0x19, 0x4E, 0x79, 0x23, 0x46, 0x23, 0x96, 0x57, 0x78, 0x23, 0x9E, 0xDA, 0x3F, 0x01, 0xB2, 0xCA,
    ///                 0x3F, 0x01, 0x56, 0x70, 0x2B, 0x5E, 0x71, 0x2B, 0x72, 0x2B, 0x73, 0x21, 0x46, 0x01, 0x34, 0x21];
    ///
    ///     assert!(binfile.add_bytes(data, Some(0x100), false).is_ok());
    ///     println!("{}", binfile.to_array_str(.., None, None)?);
    ///     Ok(())
    /// }
    /// ```
    ///
    /// ```console
    ///  0x21, 0x46, 0x01, 0x36, 0x01, 0x21, 0x47, 0x01, 0x36, 0x00, 0x7e, 0xfe, 0x09, 0xd2, 0x19, 0x01, 0x21, 0x46, 0x01, 0x7e, 0x17, 0xc2, 0x00, 0x01, 0xff, 0x5f, 0x16, 0x00, 0x21, 0x48, 0x01, 0x19, 0x19, 0x4e, 0x79, 0x23, 0x46, 0x23, 0x96, 0x57, 0x78, 0x23, 0x9e, 0xda, 0x3f, 0x01, 0xb2, 0xca, 0x3f, 0x01, 0x56, 0x70, 0x2b, 0x5e, 0x71, 0x2b, 0x72, 0x2b, 0x73, 0x21, 0x46, 0x01, 0x34, 0x21
    /// ```
    pub fn to_array_str<R>(
        &self,
        range: R,
        padding: Option<u8>,
        separator: Option<&str>,
    ) -> Result<String, Error>
    where
        R: RangeBounds<usize>,
    {
        let binary_data = self.to_bytes(range, padding)?;
        let separator = separator.unwrap_or(", ");
        let mut string = String::new();
        for value in binary_data {
            string += format!("0x{:02x}{}", value, separator).as_str();
        }
        Ok(string.trim_end_matches(separator).to_string())
    }

    /// Format the binary file as a hexdump and return it as a string array.
    ///
    /// # Examples
    ///
    /// ```rust
    /// use bin_file::BinFile;
    /// use bin_file::Error;
    ///
    /// fn main() -> Result<(), Error> {
    ///     let mut binfile = BinFile::new();
    ///     let data = [0x21, 0x46, 0x01, 0x36, 0x01, 0x21, 0x47, 0x01, 0x36, 0x00, 0x7E, 0xFE, 0x09, 0xD2, 0x19, 0x01,
    ///                 0x21, 0x46, 0x01, 0x7E, 0x17, 0xC2, 0x00, 0x01, 0xFF, 0x5F, 0x16, 0x00, 0x21, 0x48, 0x01, 0x19,
    ///                 0x19, 0x4E, 0x79, 0x23, 0x46, 0x23, 0x96, 0x57, 0x78, 0x23, 0x9E, 0xDA, 0x3F, 0x01, 0xB2, 0xCA,
    ///                 0x3F, 0x01, 0x56, 0x70, 0x2B, 0x5E, 0x71, 0x2B, 0x72, 0x2B, 0x73, 0x21, 0x46, 0x01, 0x34, 0x21];
    ///
    ///     assert!(binfile.add_bytes(data, Some(0x100), false).is_ok());
    ///     println!("{}", binfile.to_hexdump()?.join("\n"));
    ///     Ok(())
    /// }
    /// ```
    ///
    /// ```console
    /// 00000100  21 46 01 36 01 21 47 01  36 00 7e fe 09 d2 19 01  |!F.6.!G.6.~.....|
    /// 00000110  21 46 01 7e 17 c2 00 01  ff 5f 16 00 21 48 01 19  |!F.~....._..!H..|
    /// 00000120  19 4e 79 23 46 23 96 57  78 23 9e da 3f 01 b2 ca  |.Ny#F#.Wx#..?...|
    /// 00000130  3f 01 56 70 2b 5e 71 2b  72 2b 73 21 46 01 34 21  |?.Vp+^q+r+s!F.4!|
    /// ```
    pub fn to_hexdump(&self) -> Result<Vec<String>, Error> {
        if self.is_empty() {
            Ok(Vec::new())
        } else {
            let not_dot_characters = PRINTABLE;
            let align_to_line = |address: usize| address - (address % 16);
            let padding = |length: usize| -> Vec<Option<u8>> { vec![None; length] };
            let format_line = |address: usize, data: &Vec<Option<u8>>| -> String {
                let mut data = data.clone();
                let mut padding = padding(16 - data.len());
                data.append(&mut padding);

                let mut hexdata = Vec::new();
                for byte in &data {
                    let elem = if let Some(byte) = byte {
                        format!("{:02x}", byte)
                    } else {
                        "  ".to_string()
                    };
                    hexdata.push(elem);
                }
                let first_half = hexdata[..8].join(" ");
                let second_half = hexdata[8..].join(" ");
                let mut text = String::new();

                for byte in data {
                    if let Some(byte) = byte {
                        if not_dot_characters.contains(byte as char) {
                            text += format!("{}", byte as char).as_str();
                        } else {
                            text += "."
                        }
                    } else {
                        text += " "
                    }
                }
                format!(
                    "{:08x}  {:23}  {:23}  |{:16}|",
                    address, first_half, second_half, text
                )
            };
            let mut lines = Vec::new();
            let mut line_address = align_to_line(self.minimum_address().unwrap());
            let mut line_data = Vec::new();

            for (address, data) in self.segments.chunks(Some(16), Some(16))? {
                let aligned_chunk_address = align_to_line(address);
                if aligned_chunk_address > line_address {
                    lines.push(format_line(line_address, &line_data));
                    if aligned_chunk_address > line_address + 16 {
                        lines.push("...".to_string());
                    }
                    line_address = aligned_chunk_address;
                    line_data.clear();
                }
                let mut padding = padding(address - line_address - line_data.len());
                line_data.append(&mut padding);
                for value in data {
                    line_data.push(Some(value));
                }
            }
            lines.push(format_line(line_address, &line_data));
            Ok(lines)
        }
    }

    /// Fill empty space between segments.
    ///
    /// # Parameters
    ///
    /// - `value`: is the value which is used to fill the empty space. By
    ///   default the value is `0xff`.
    /// - `max_words` is the maximum number of words to fill between the
    ///   segments. Empty space which larger than this is not
    ///   touched. If `None`, all empty space is filled.
    pub fn fill(&mut self, value: Option<u8>, max_words: Option<usize>) -> Result<(), Error> {
        let value = value.unwrap_or(0xff);
        let mut previous_segment_maximum_address = None;
        let mut fill_segments = Vec::new();

        for segment in self.segments.segments() {
            let address = segment.minimum_address();
            let maximum_address = address + segment.len();

            if let Some(previous_segment_maximum_address) = previous_segment_maximum_address {
                let fill_size = address - previous_segment_maximum_address;

                if max_words.is_none() || fill_size <= max_words.unwrap() {
                    fill_segments.push(Segment::new(
                        previous_segment_maximum_address,
                        vec![value; fill_size],
                    ));
                }
            }
            previous_segment_maximum_address = Some(maximum_address);
        }
        for segment in fill_segments {
            self.segments.add_segment(segment, false)?;
        }
        Ok(())
    }

    /// Exclude given range and keep the rest.
    ///
    /// # Parameters
    ///
    /// - `range`: Range, which should be excluded
    pub fn exclude(&mut self, range: Range<usize>) -> Result<(), Error> {
        if range.end < range.start {
            Err(Error::InvalidAddressRange)
        } else {
            self.segments.remove(range);
            Ok(())
        }
    }

    /// Keep given range and discard the rest.
    ///
    /// # Parameters
    ///
    /// - `range`: Range, which shall be kept
    pub fn crop(&mut self, range: Range<usize>) -> Result<(), Error> {
        let maximum_address = self.segments.get_maximum_address().unwrap();
        self.segments.remove(0..range.start);
        self.segments.remove(range.end..maximum_address);
        Ok(())
    }

    /// Length of all bytes in all segments
    pub fn len(&self) -> usize {
        let mut len = 0;
        for segment in self.segments.segments() {
            len += segment.len()
        }
        len
    }

    /// Returns true if datas are empty
    pub fn is_empty(&self) -> bool {
        self.segments.segments().is_empty()
    }

    /// Return a string of human readable information about the binary
    /// file.
    ///
    /// # Examples
    ///
    /// ```rust
    /// use bin_file::BinFile;
    /// use bin_file::Error;
    ///
    /// fn main() -> Result<(), Error> {
    ///     let mut binfile = BinFile::new();
    ///     let data = [0x21, 0x46, 0x01, 0x36, 0x01, 0x21, 0x47, 0x01, 0x36, 0x00, 0x7E, 0xFE, 0x09, 0xD2, 0x19, 0x01,
    ///                 0x21, 0x46, 0x01, 0x7E, 0x17, 0xC2, 0x00, 0x01, 0xFF, 0x5F, 0x16, 0x00, 0x21, 0x48, 0x01, 0x19,
    ///                 0x19, 0x4E, 0x79, 0x23, 0x46, 0x23, 0x96, 0x57, 0x78, 0x23, 0x9E, 0xDA, 0x3F, 0x01, 0xB2, 0xCA,
    ///                 0x3F, 0x01, 0x56, 0x70, 0x2B, 0x5E, 0x71, 0x2B, 0x72, 0x2B, 0x73, 0x21, 0x46, 0x01, 0x34, 0x21];
    ///
    ///     assert!(binfile.add_bytes(data, Some(0x100), false).is_ok());
    ///     println!("{}", binfile.info());
    ///     Ok(())
    /// }
    /// ```
    pub fn info(&self) -> String {
        let mut info = String::new();
        if let Some(header) = self.header.as_ref() {
            info += format!(
                "Header:                  \"{}\"\n",
                String::from_utf8(header.to_vec()).unwrap()
            )
            .as_str()
        }
        if let Some(execution_start_address) = self.execution_start_address {
            info += format!(
                "Execution start address: 0x{:08x}\n",
                execution_start_address
            )
            .as_str()
        }
        info += "Data ranges:\n\n";
        for segment in self.segments() {
            let minimum_address = segment.minimum_address();
            let size = segment.len();
            let maximum_address = minimum_address + size;
            info += format!(
                "    0x{:08x} - 0x{:08x} ({})\n",
                minimum_address,
                maximum_address,
                ByteSize(size as u64)
            )
            .as_str();
        }
        info
    }

    /// Return the memory layout as a string.
    ///
    /// # Examples
    ///
    /// ```rust
    /// use bin_file::BinFile;
    /// use bin_file::Error;
    ///
    /// fn main() -> Result<(), Error> {
    ///     let mut binfile = BinFile::new();
    ///     let data = [0x21, 0x46, 0x01, 0x36, 0x01, 0x21, 0x47, 0x01, 0x36, 0x00, 0x7E, 0xFE, 0x09, 0xD2, 0x19, 0x01,
    ///                 0x21, 0x46, 0x01, 0x7E, 0x17, 0xC2, 0x00, 0x01, 0xFF, 0x5F, 0x16, 0x00, 0x21, 0x48, 0x01, 0x19,
    ///                 0x19, 0x4E, 0x79, 0x23, 0x46, 0x23, 0x96, 0x57, 0x78, 0x23, 0x9E, 0xDA, 0x3F, 0x01, 0xB2, 0xCA,
    ///                 0x3F, 0x01, 0x56, 0x70, 0x2B, 0x5E, 0x71, 0x2B, 0x72, 0x2B, 0x73, 0x21, 0x46, 0x01, 0x34, 0x21];
    ///
    ///     assert!(binfile.add_bytes(data, Some(0x100), false).is_ok());
    ///     println!("{}", binfile.layout()?);
    ///     Ok(())
    /// }
    /// ```
    ///
    /// ```console
    /// 0x100                                                      0x140
    /// ================================================================
    /// ```
    pub fn layout(&self) -> Result<String, Error> {
        if let (Some(minimum_address), Some(maximum_address)) =
            (self.minimum_address(), self.maximum_address())
        {
            let size = maximum_address - minimum_address;
            let width = min(80, size);
            let mut chunk_address = minimum_address;
            let chunk_size = size / width;
            let minimum_address = format!("0x{:x}", minimum_address);
            let maximum_address = format!("0x{:x}", maximum_address);
            let padding = " ".repeat(width - minimum_address.len() - maximum_address.len());
            let mut output = format!("{}{}{}\n", minimum_address, padding, maximum_address);
            for i in 0..width {
                let mut chunk = BinFile::try_from(self)?;

                let maximum_address = if i < width - 1 {
                    chunk_address + chunk_size
                } else {
                    chunk.maximum_address().unwrap()
                };
                chunk.crop(chunk_address..maximum_address)?;

                if chunk.is_empty() {
                    output += " ";
                } else if chunk.len() != maximum_address - chunk_address {
                    output += "-";
                } else {
                    output += "=";
                }
                chunk_address += chunk_size;
            }
            Ok(output)
        } else {
            Ok("Empty File".into())
        }
    }
}

impl Default for BinFile {
    fn default() -> Self {
        Self::new()
    }
}

impl TryFrom<PathBuf> for BinFile {
    type Error = Error;

    fn try_from(file_name: PathBuf) -> Result<Self, Self::Error> {
        Self::from_file(file_name)
    }
}

impl TryFrom<&PathBuf> for BinFile {
    type Error = Error;

    fn try_from(file_name: &PathBuf) -> Result<Self, Self::Error> {
        Self::from_file(file_name)
    }
}

impl TryFrom<&Path> for BinFile {
    type Error = Error;

    fn try_from(file_name: &Path) -> Result<Self, Self::Error> {
        Self::from_file(file_name)
    }
}

impl TryFrom<&BinFile> for BinFile {
    type Error = Error;
    fn try_from(value: &BinFile) -> Result<Self, Self::Error> {
        let mut binfile = BinFile::new();
        binfile.add_srec(value.to_srec(None, SRecordAddressLength::default())?, false)?;
        Ok(binfile)
    }
}

impl Add for BinFile {
    type Output = Self;

    fn add(self, rhs: Self) -> Self::Output {
        let mut binfile = self;
        let _ = binfile.add_srec(
            rhs.to_srec(None, SRecordAddressLength::default()).unwrap(),
            false,
        );
        binfile
    }
}

impl AddAssign for BinFile {
    fn add_assign(&mut self, rhs: Self) {
        let _ = self.add_srec(
            rhs.to_srec(None, SRecordAddressLength::default()).unwrap(),
            false,
        );
    }
}

impl Add<&[String]> for BinFile {
    type Output = Self;

    fn add(self, rhs: &[String]) -> Self::Output {
        let mut binfile = self;
        let _ = binfile.add_strings(rhs, false);
        binfile
    }
}

impl AddAssign<&[String]> for BinFile {
    fn add_assign(&mut self, rhs: &[String]) {
        let _ = self.add_strings(rhs, false);
    }
}

impl Display for BinFile {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.segments)
    }
}

impl Read for BinFile {
    fn read(&mut self, buf: &mut [u8]) -> std::io::Result<usize> {
        if self.is_empty() {
            Ok(0)
        } else {
            let minimum_address = max(self.minimum_address().unwrap(), self.cursor);
            let max_buffer_address = minimum_address + buf.len();
            if max_buffer_address < self.maximum_address().unwrap() {
                self.cursor = max_buffer_address;
            } else {
                self.cursor = 0;
            }
            let maximum_address = min(max_buffer_address, self.maximum_address().unwrap());
            match self.to_bytes(minimum_address..maximum_address, None) {
                Ok(values) => {
                    for (pos, val) in values.iter().enumerate() {
                        buf[pos] = *val;
                    }
                    Ok(maximum_address - minimum_address)
                }
                Err(err) => Err(std::io::Error::new(std::io::ErrorKind::InvalidData, err)),
            }
        }
    }
}

impl Write for BinFile {
    fn write(&mut self, buf: &[u8]) -> std::io::Result<usize> {
        match self.add_bytes(buf, None, false) {
            Ok(_) => Ok(buf.len()),
            Err(err) => Err(std::io::Error::new(std::io::ErrorKind::InvalidData, err)),
        }
    }

    fn flush(&mut self) -> std::io::Result<()> {
        Ok(())
    }
}

impl FromStr for BinFile {
    type Err = Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut binfile = BinFile {
            header: None,
            execution_start_address: None,
            segments: Segments::new(),
            cursor: 0,
        };
        let lines: Vec<&str> = s.lines().collect();
        binfile.add_strings(lines, false)?;
        Ok(binfile)
    }
}

fn comment_remover(data: &str) -> String {
    let regex =
        regex::RegexBuilder::new(r#"//.*?$|/\*.*?\*/|\'(?:\\.|[^\\\'])*\'|"(?:\\.|[^\\"])*""#)
            .dot_matches_new_line(true)
            .multi_line(true)
            .build()
            .unwrap();
    let replacer = |caps: &Captures| -> String {
        let s = caps.get(0).unwrap();
        if s.as_str().starts_with('/') {
            " ".to_string()
        } else {
            s.as_str().to_string()
        }
    };
    regex.replace_all(data, &replacer).to_string()
}

fn is_verilog_vmem<T, S>(data: T) -> bool
where
    T: AsRef<[S]>,
    S: AsRef<str>,
{
    BinFile {
        header: None,
        execution_start_address: None,
        segments: Segments::new(),
        cursor: 0,
    }
    .add_verilog_vmem(data, false)
    .is_ok()
}

fn is_ti_txt<T, S>(data: T) -> bool
where
    T: AsRef<[S]>,
    S: AsRef<str>,
{
    BinFile {
        header: None,
        execution_start_address: None,
        segments: Segments::new(),
        cursor: 0,
    }
    .add_ti_txt(data, false)
    .is_ok()
}

#[cfg(test)]
mod tests {

    use super::*;

    fn open_text_file(file_name: &str) -> Vec<String> {
        let file = File::open(file_name).unwrap();
        let reader = BufReader::new(file);
        let data: Vec<String> = reader.lines().map_while(Result::ok).collect();
        data
    }

    fn open_binary_file(file_name: &str) -> Vec<u8> {
        let mut file = File::open(file_name).unwrap();
        let metadata = metadata(file_name).unwrap();
        let mut data: Vec<u8> = vec![0; metadata.len() as usize];
        let n = file.read(&mut data);
        data[..n.unwrap()].to_vec()
    }

    #[test]
    fn it_works() {
        let binfile = BinFile::new();
        print!("{}", binfile.info())
    }

    #[test]
    fn it_fails_with_report() {
        let mut binfile = BinFile::new();
        assert!(binfile
            .add_binary_file("/dev/this surely doesnt exist", None, false)
            .is_err())
    }

    #[test]
    fn test_srec() {
        let data = open_text_file("tests/in.s19");
        let mut binfile = BinFile::new();
        assert!(binfile.add_srec(&data, false).is_ok());
        assert_eq!(
            binfile
                .to_srec(Some(28), SRecordAddressLength::Length16)
                .unwrap(),
            data
        );

        let data = open_binary_file("tests/empty_main.bin");
        let mut binfile = BinFile::new();
        assert!(binfile.add_srec_file("tests/empty_main.s19", false).is_ok());
        let mut binfile2 = BinFile::new();
        assert!(binfile2
            .add_srec_file("tests/empty_main.s19", false)
            .is_ok());
        assert!(binfile2
            .add_srec_file("tests/empty_main_rearranged.s19", true)
            .is_ok());
        assert_eq!(binfile.to_bytes(.., Some(0x00)).unwrap().len(), data.len());
        assert_eq!(binfile.to_bytes(.., Some(0x00)).unwrap(), data);
        assert_eq!(binfile2.to_bytes(.., Some(0x00)).unwrap().len(), data.len());
        assert_eq!(binfile2.to_bytes(.., Some(0x00)).unwrap(), data);

        let mut binfile = BinFile::new();
        let res = binfile.add_srec_file("tests/bad_crc.s19", false);
        assert!(res.is_err());
        assert_eq!(res, Err(Error::ChecksumMismatch(0x22, 0x25)));
    }

    #[test]
    fn test_ti_txt() {
        let data = open_text_file("tests/in.s19.txt");
        let mut binfile = BinFile::new();
        assert!(binfile.add_ti_txt(&data, false).is_ok());
        assert_eq!(binfile.to_ti_txt().unwrap(), data);

        let data = open_binary_file("tests/empty_main.bin");
        let mut binfile = BinFile::new();
        assert!(binfile
            .add_ti_txt_file("tests/empty_main.s19.txt", false)
            .is_ok());
        let mut binfile2 = BinFile::new();
        assert!(binfile2
            .add_ti_txt_file("tests/empty_main.s19.txt", false)
            .is_ok());
        assert!(binfile2
            .add_ti_txt_file("tests/empty_main_rearranged.s19.txt", true)
            .is_ok());
        assert_eq!(binfile.to_bytes(.., Some(0x00)).unwrap().len(), data.len());
        assert_eq!(binfile.to_bytes(.., Some(0x00)).unwrap(), data);
        assert_eq!(binfile2.to_bytes(.., Some(0x00)).unwrap().len(), data.len());
        assert_eq!(binfile2.to_bytes(.., Some(0x00)).unwrap(), data);

        let empty = BinFile::new();
        let mut binfile = BinFile::new();
        assert!(binfile.add_ti_txt_file("tests/empty.txt", false).is_ok());
        assert_eq!(empty.to_ti_txt(), binfile.to_ti_txt());
    }

    #[test]
    fn test_bad_ti_txt() {
        let datas = [
            (
                "bad_ti_txt_address_value.txt",
                Error::ContainsInvalidCharacters,
            ),
            ("bad_ti_txt_bad_q.txt", Error::UnsupportedFileFormat),
            (
                "bad_ti_txt_data_value.txt",
                Error::ContainsInvalidCharacters,
            ),
            ("bad_ti_txt_record_short.txt", Error::MissingStartCode),
            ("bad_ti_txt_record_long.txt", Error::RecordTooLong),
            ("bad_ti_txt_no_offset.txt", Error::MissingStartCode),
            ("bad_ti_txt_no_q.txt", Error::UnsupportedFileFormat),
            ("bad_ti_txt_blank_line.txt", Error::RecordTooShort),
        ];
        for (filename, error) in datas {
            let mut binfile = BinFile::new();
            let res = binfile.add_ti_txt_file(format!("tests/{}", filename), false);
            assert_eq!(res, Err(error), "Testing: {}", filename);
        }
    }

    #[test]
    fn test_compare_ti_txt() {
        let filenames = [
            "in.s19",
            "empty_main.s19",
            "convert.s19",
            "out.s19",
            //            "non_sorted_segments.s19",
            "non_sorted_segments_merged_and_sorted.s19",
            "in.hex",
            "empty_main.hex",
            "convert.hex",
            "out.hex",
        ];

        for file_1 in filenames {
            let file_2 = format!("{}.txt", file_1);
            let mut binfile1 = BinFile::new();
            let mut binfile2 = BinFile::new();
            let res1 = binfile1.add_file(format!("tests/{}", file_1), false);

            assert_eq!(
                res1.is_ok(),
                binfile2
                    .add_file(format!("tests/{}", file_2), false)
                    .is_ok(),
                "Testing: {}",
                file_1
            );
            assert_eq!(
                binfile1.to_ti_txt(),
                binfile2.to_ti_txt(),
                "Testing: {}",
                file_1
            )
        }
    }

    #[test]
    fn test_hex() {
        let data = open_text_file("tests/in.hex");
        let mut binfile = BinFile::new();
        assert!(binfile.add_ihex(&data, false).is_ok());
        assert_eq!(binfile.to_ihex(None, IHexFormat::default()).unwrap(), data);

        let mut binfile = BinFile::new();
        assert!(binfile.add_ihex_file("tests/in.hex", false).is_ok());
        assert!(binfile.add_ihex_file("tests/in.hex", true).is_ok());
        assert_eq!(binfile.to_ihex(None, IHexFormat::default()).unwrap(), data);
    }

    #[test]
    fn test_i8hex() {
        let mut binfile = BinFile::new();

        let data = [
            ":0100000001FE".to_string(),
            ":0101000002FC".to_string(),
            ":01FFFF0003FE".to_string(),
            // Will not be part of I8HEX output.
            ":0400000300000000F9".to_string(),
            ":00000001FF".to_string(),
        ];
        assert!(binfile.add_ihex(&data, false).is_ok());
        assert_eq!(binfile.segments().len(), 3);
        assert_eq!(binfile.segments()[0].get_tuple(), (0, &vec![0x01]));
        assert_eq!(binfile.segments()[1].get_tuple(), (0x100, &vec![0x02]));
        assert_eq!(binfile.segments()[2].get_tuple(), (0xffff, &vec![0x03]));
        let data_exp = [
            ":0100000001FE".to_string(),
            ":0101000002FC".to_string(),
            ":01FFFF0003FE".to_string(),
            ":00000001FF".to_string(),
        ];
        assert_eq!(
            binfile.to_ihex(None, IHexFormat::IHex8),
            Ok(data_exp.to_vec())
        );
    }

    #[test]
    fn test_i8hex_address_above_64k() {
        let mut binfile = BinFile::new();

        assert!(binfile.add_bytes([0x00], Some(65536), false).is_ok());
        let res = binfile.to_ihex(None, IHexFormat::IHex8);
        assert_eq!(res, Err(Error::AddressTooBig));
    }

    #[test]
    fn test_i16hex() {
        let mut binfile = BinFile::new();

        let data = [
            ":0100000001FE".to_string(),
            ":01F00000020D".to_string(),
            ":01FFFF0003FE".to_string(),
            ":02000002C0003C".to_string(),
            ":0110000005EA".to_string(),
            ":02000002FFFFFE".to_string(),
            ":0100000006F9".to_string(),
            ":01FFFF0007FA".to_string(),
            ":020000021000EC".to_string(),
            ":0100000004FB".to_string(),
            // Coverted to 03 in I16HEX output
            ":0400000500000000F7".to_string(),
            ":00000001FF".to_string(),
        ];
        assert!(binfile.add_ihex(&data, false).is_ok());
        assert_eq!(binfile.segments().len(), 6);
        assert_eq!(binfile.segments()[0].get_tuple(), (0, &vec![0x01]));
        assert_eq!(binfile.segments()[1].get_tuple(), (0xf000, &vec![0x02]));
        assert_eq!(
            binfile.segments()[2].get_tuple(),
            (0xffff, &vec![0x03, 0x04])
        );
        assert_eq!(
            binfile.segments()[3].get_tuple(),
            (16 * 0xc000 + 0x1000, &vec![0x05])
        );
        assert_eq!(
            binfile.segments()[4].get_tuple(),
            (16 * 0xffff, &vec![0x06])
        );
        assert_eq!(
            binfile.segments()[5].get_tuple(),
            (17 * 0xffff, &vec![0x07])
        );
        let data_exp = [
            ":0100000001FE".to_string(),
            ":01F00000020D".to_string(),
            ":02FFFF000304F9".to_string(),
            ":02000002C0003C".to_string(),
            ":0110000005EA".to_string(),
            ":02000002F0000C".to_string(),
            ":01FFF000060A".to_string(),
            ":02000002FFFFFE".to_string(),
            ":01FFFF0007FA".to_string(),
            ":0400000300000000F9".to_string(),
            ":00000001FF".to_string(),
        ];
        assert_eq!(
            binfile.to_ihex(None, IHexFormat::IHex16),
            Ok(data_exp.to_vec())
        );
    }

    #[test]
    fn test_i16hex_address_above_1meg() {
        let mut binfile = BinFile::new();

        assert!(binfile
            .add_bytes([0x00], Some(17 * 65535 + 1), false)
            .is_ok());
        let res = binfile.to_ihex(None, IHexFormat::IHex16);
        assert_eq!(res, Err(Error::AddressTooBig));
    }

    #[test]
    fn test_i32hex() {
        let mut binfile = BinFile::new();

        let data = [
            ":0100000001FE".to_string(),
            ":01FFFF0002FF".to_string(),
            ":02000004FFFFFC".to_string(),
            ":0100000004FB".to_string(),
            ":01FFFF0005FC".to_string(),
            ":020000040001F9".to_string(),
            ":0100000003FC".to_string(),
            ":0400000500000000F7".to_string(),
            ":00000001FF".to_string(),
        ];
        assert!(binfile.add_ihex(&data, false).is_ok());
        let data_exp = [
            ":0100000001FE".to_string(),
            ":02FFFF000203FB".to_string(),
            ":02000004FFFFFC".to_string(),
            ":0100000004FB".to_string(),
            ":01FFFF0005FC".to_string(),
            ":0400000500000000F7".to_string(),
            ":00000001FF".to_string(),
        ];
        assert_eq!(
            binfile.to_ihex(None, IHexFormat::IHex32),
            Ok(data_exp.to_vec())
        );
        assert_eq!(binfile.minimum_address(), Some(0));
        assert_eq!(binfile.maximum_address(), Some(0x100000000));
        assert_eq!(binfile.execution_start_address(), Some(0));
        assert_eq!(binfile.get_value_by_address(0), Some(1));
        assert_eq!(binfile.get_value_by_address(0xffff), Some(2));
        assert_eq!(binfile.get_value_by_address(0x10000), Some(3));
        assert_eq!(binfile.get_value_by_address(0xffff0000), Some(4));
        assert_eq!(binfile.get_value_by_address(0xffff0002), None);
        assert_eq!(binfile.get_value_by_address(0xffff0004), None);
        assert_eq!(binfile.get_value_by_address(0xffffffff), Some(0x05));
    }

    #[test]
    fn test_i32hex_address_above_4gig() {
        let mut binfile = BinFile::new();

        assert!(binfile.add_bytes([0x00], Some(0x100000000), false).is_ok());
        let res = binfile.to_ihex(None, IHexFormat::IHex32);
        assert_eq!(res, Err(Error::AddressTooBig));
    }

    #[test]
    fn test_binary() {
        let data = open_binary_file("tests/binary1.bin");
        let mut binfile = BinFile::new();
        assert!(binfile.add_bytes(&data, None, false).is_ok());
        assert_eq!(binfile.to_bytes(.., None), Ok(data));

        // Add and overwrite data to 15..179
        let mut binfile = BinFile::new();
        assert!(binfile
            .add_binary_file("tests/binary2.bin", Some(15), false)
            .is_ok());
        assert!(binfile
            .add_binary_file("tests/binary2.bin", Some(15), true)
            .is_ok());
        let data = open_binary_file("tests/binary2.bin");
        assert_eq!(
            binfile.add_bytes(&data, Some(20), false),
            Err(Error::AddDataError),
        );

        // Exclude the overlapping part and add
        assert!(binfile.exclude(20..1024).is_ok());
        assert!(binfile.add_bytes(&data, Some(20), false).is_ok());

        let mut data = open_binary_file("tests/binary3.bin");
        assert_eq!(binfile.to_bytes(0.., Some(0x00)), Ok(data.clone()));

        // Exclude first byte and read it to test adjecent add before
        assert!(binfile.exclude(0..1).is_ok());
        assert!(binfile.add_bytes([1], None, false).is_ok());
        let first = data.first_mut().unwrap();
        *first = 1;
        assert_eq!(binfile.to_bytes(0.., Some(0x00)), Ok(data.clone()));

        // Basic checks
        assert_eq!(binfile.minimum_address(), Some(0));
        assert_eq!(binfile.maximum_address(), Some(184));
        assert_eq!(binfile.len(), 170);

        // Dump with start address beyond end of binary
        assert_eq!(binfile.to_bytes(512.., None), Ok(vec![]));

        // Dump with start address at maximum address
        assert_eq!(binfile.to_bytes(184.., None), Ok(vec![]));

        // Dump with start address one before maximum address
        assert_eq!(binfile.to_bytes(183.., None), Ok(vec![10]));

        // Dump with start address one after minimum address
        assert_eq!(binfile.to_bytes(1.., Some(0)), Ok(data[1..].to_vec()));

        // Dump with start address 16 and end address 18
        assert_eq!(binfile.to_bytes(16..18, None), Ok(vec![0x32, 0x30]));

        // Dump with start address and end address 16
        assert_eq!(binfile.to_bytes(16..16, None), Ok(vec![]));

        // Dump with end beyond end of binary
        assert_eq!(binfile.to_bytes(..1024, Some(0)), Ok(data));

        // Dump with end before start
        assert_eq!(binfile.to_bytes(2..0, None), Ok(vec![]));
    }

    #[test]
    fn test_add() {
        let mut binfile = BinFile::new();
        let data = open_text_file("tests/in.s19");
        let binfile_2 = binfile.clone() + data.as_slice();
        binfile += data.as_slice();
        assert_eq!(
            binfile.to_srec(Some(28), SRecordAddressLength::Length16),
            Ok(data.clone())
        );
        assert_eq!(
            binfile_2.to_srec(Some(28), SRecordAddressLength::Length16),
            Ok(data)
        );

        let mut binfile = BinFile::new();
        let data = open_text_file("tests/in.hex");
        binfile += data.as_slice();
        assert_eq!(binfile.to_ihex(None, IHexFormat::default()), Ok(data));
    }

    #[test]
    fn test_add_strings() {
        let mut binfile = BinFile::new();
        let data = open_text_file("tests/in.s19");
        assert!(binfile.add_strings(data.as_slice(), false).is_ok());
        assert_eq!(
            binfile.to_srec(Some(28), SRecordAddressLength::Length16),
            Ok(data)
        );

        let mut binfile = BinFile::new();
        let data = open_text_file("tests/in.hex");
        assert!(binfile.add_strings(data.as_slice(), false).is_ok());
        assert_eq!(binfile.to_ihex(None, IHexFormat::default()), Ok(data));

        let mut binfile = BinFile::new();
        let res = binfile.add_strings(["invalid data"], false);
        assert_eq!(res, Err(Error::UnsupportedFileFormat));

        let mut binfile = BinFile::new();
        let res = binfile.add_strings(
            [
                "S214400420ED044000E8B7FFFFFFF4660F1F440000EE",
                "invalid data",
            ],
            false,
        );
        assert_eq!(res, Err(Error::MissingStartCode));

        let mut binfile = BinFile::new();
        let res = binfile.add_strings([":020000040040BA", "invalid data"], false);
        assert_eq!(res, Err(Error::MissingStartCode));
    }

    #[test]
    fn test_add_file() {
        let mut binfile = BinFile::new();
        assert!(binfile
            .add_file("tests/empty_main_rearranged.s19", false)
            .is_ok());
        let data = open_binary_file("tests/empty_main.bin");
        assert_eq!(binfile.to_bytes(.., Some(0x00)), Ok(data));

        let mut binfile = BinFile::new();
        assert!(binfile.add_file("tests/in.hex", false).is_ok());
        let data = open_text_file("tests/in.hex");
        assert_eq!(binfile.to_ihex(None, IHexFormat::default()), Ok(data));

        let mut binfile = BinFile::new();
        assert_eq!(
            binfile.add_file("tests/hexdump.txt", false),
            Err(Error::UnsupportedFileFormat)
        );
    }

    #[test]
    fn test_from_file() {
        let binfile = BinFile::from_file("tests/empty_main_rearranged.s19").unwrap();
        let data = open_binary_file("tests/empty_main.bin");
        assert_eq!(binfile.to_bytes(.., Some(0x00)), Ok(data));

        assert_eq!(
            BinFile::from_file("tests/hexdump.txt").err(),
            Some(Error::UnsupportedFileFormat)
        );

        let binfile =
            BinFile::try_from(PathBuf::from("tests/empty_main_rearranged.s19").as_path()).unwrap();
        let data = open_binary_file("tests/empty_main.bin");
        assert_eq!(binfile.to_bytes(.., Some(0x00)), Ok(data));

        assert_eq!(
            BinFile::from_file("tests/hexdump.txt").err(),
            Some(Error::UnsupportedFileFormat)
        );
    }

    #[test]
    fn test_from_files() {
        let binfile = BinFile::from_files(["tests/in.hex", "tests/in.hex"], true).unwrap();
        let data = open_text_file("tests/in.hex");
        assert_eq!(binfile.to_ihex(None, IHexFormat::default()), Ok(data));
    }

    #[test]
    fn test_array() {
        let mut binfile = BinFile::new();
        assert!(binfile.add_ihex_file("tests/in.hex", false).is_ok());
        let data = open_text_file("tests/in.i");
        assert_eq!(binfile.to_array_str(.., None, None), Ok(data[0].to_owned()));
    }

    #[test]
    fn test_hexdump_1() {
        let mut binfile = BinFile::new();
        assert!(binfile.add_bytes("12".as_bytes(), Some(17), false).is_ok());
        assert!(binfile.add_bytes("34".as_bytes(), Some(26), false).is_ok());
        assert!(binfile
            .add_bytes("5678".as_bytes(), Some(30), false)
            .is_ok());
        assert!(binfile.add_bytes("9".as_bytes(), Some(47), false).is_ok());
        let data = open_text_file("tests/hexdump.txt");
        assert_eq!(binfile.to_hexdump(), Ok(data));
    }

    #[test]
    fn test_hexdump_2() {
        let mut binfile = BinFile::new();
        assert!(binfile
            .add_bytes("34".as_bytes(), Some(0x150), false)
            .is_ok());
        assert!(binfile
            .add_bytes("3".as_bytes(), Some(0x163), false)
            .is_ok());
        assert!(binfile.add_bytes([0x01], Some(0x260), false).is_ok());
        assert!(binfile
            .add_bytes("3".as_bytes(), Some(0x263), false)
            .is_ok());
        let data = open_text_file("tests/hexdump2.txt");
        assert_eq!(binfile.to_hexdump(), Ok(data));
    }

    #[test]
    fn test_hexdump_gaps() {
        let mut binfile = BinFile::new();
        assert!(binfile.add_bytes("1".as_bytes(), Some(0), false).is_ok());
        assert!(binfile.add_bytes("3".as_bytes(), Some(32), false).is_ok());
        assert!(binfile.add_bytes("6".as_bytes(), Some(80), false).is_ok());
        let data = open_text_file("tests/hexdump3.txt");
        assert_eq!(binfile.to_hexdump(), Ok(data));
    }

    #[test]
    fn test_hexdump_empty() {
        let binfile = BinFile::new();
        assert_eq!(binfile.to_hexdump(), Ok(vec![]));
    }

    #[test]
    fn test_srec_ihex_binary() {
        let mut binfile = BinFile::new();
        let data = open_text_file("tests/in.hex");
        assert!(binfile.add_ihex(data, false).is_ok());
        let data = open_text_file("tests/in.s19");
        assert!(binfile.add_srec(data, false).is_ok());
        let data = open_binary_file("tests/binary1.bin");
        assert!(binfile.add_bytes(data, Some(1024), false).is_ok());
        let data = open_text_file("tests/out.hex");
        assert_eq!(binfile.to_ihex(None, IHexFormat::default()), Ok(data));
        let data = open_text_file("tests/out.s19");
        assert_eq!(
            binfile.to_srec(None, SRecordAddressLength::Length16),
            Ok(data)
        );
        assert!(binfile.fill(Some(0x00), None).is_ok());
        let data = open_binary_file("tests/out.bin");
        assert_eq!(binfile.to_bytes(.., None), Ok(data));
    }

    #[test]
    fn test_exclude() {
        let mut binfile = BinFile::new();
        assert!(binfile.add_file("tests/in.s19", false).is_ok());
        assert!(binfile.exclude(2..4).is_ok());
        let data = open_text_file("tests/in_exclude_2_4.s19");
        assert_eq!(
            binfile.to_srec(Some(32), SRecordAddressLength::Length16),
            Ok(data)
        );

        let mut binfile = BinFile::new();
        assert!(binfile.add_file("tests/in.s19", false).is_ok());
        assert!(binfile.exclude(3..1024).is_ok());
        let data = open_text_file("tests/in_exclude_3_1024.s19");
        assert_eq!(
            binfile.to_srec(Some(32), SRecordAddressLength::Length16),
            Ok(data)
        );

        let mut binfile = BinFile::new();
        assert!(binfile.add_file("tests/in.s19", false).is_ok());
        assert!(binfile.exclude(0..9).is_ok());
        let data = open_text_file("tests/in_exclude_0_9.s19");
        assert_eq!(
            binfile.to_srec(Some(32), SRecordAddressLength::Length16),
            Ok(data)
        );

        let mut binfile = BinFile::new();
        assert!(binfile.add_file("tests/empty_main.s19", false).is_ok());
        assert!(binfile.exclude(0x400240..0x400600).is_ok());
        let data = open_binary_file("tests/empty_main_mod.bin");
        assert_eq!(binfile.to_bytes(.., Some(0x00)), Ok(data));

        let mut binfile = BinFile::new();
        assert!(binfile
            .add_bytes("111111".as_bytes(), Some(8), false)
            .is_ok());
        assert!(binfile
            .add_bytes("222222".as_bytes(), Some(16), false)
            .is_ok());
        assert!(binfile
            .add_bytes("333333".as_bytes(), Some(24), false)
            .is_ok());
        assert!(binfile.exclude(7..8).is_ok());
        assert!(binfile.exclude(15..16).is_ok());
        assert!(binfile.exclude(23..24).is_ok());
        let data = vec![
            "111111".as_bytes().to_owned(),
            vec![0xff; 2],
            "222222".as_bytes().to_owned(),
            vec![0xff; 2],
            "333333".as_bytes().to_owned(),
        ]
        .into_iter()
        .flatten()
        .collect();
        assert_eq!(binfile.to_bytes(.., None), Ok(data));
        assert_eq!(binfile.segments().len(), 3);

        assert!(binfile.exclude(20..24).is_ok());
        let data = vec![
            "111111".as_bytes().to_owned(),
            vec![0xff; 2],
            "2222".as_bytes().to_owned(),
            vec![0xff; 4],
            "333333".as_bytes().to_owned(),
        ]
        .into_iter()
        .flatten()
        .collect();
        assert_eq!(binfile.to_bytes(.., None), Ok(data));
        assert_eq!(binfile.segments().len(), 3);

        assert!(binfile.exclude(12..24).is_ok());
        let data = vec![
            "1111".as_bytes().to_owned(),
            vec![0xff; 12],
            "333333".as_bytes().to_owned(),
        ]
        .into_iter()
        .flatten()
        .collect();
        assert_eq!(binfile.to_bytes(.., None), Ok(data));
        assert_eq!(binfile.segments().len(), 2);

        assert!(binfile.exclude(11..26).is_ok());
        let data = vec![
            "111".as_bytes().to_owned(),
            vec![0xff; 15],
            "3333".as_bytes().to_owned(),
        ]
        .into_iter()
        .flatten()
        .collect();
        assert_eq!(binfile.to_bytes(.., None), Ok(data));
        assert_eq!(binfile.segments().len(), 2);

        assert!(binfile.exclude(27..29).is_ok());
        let data = vec![
            "111".as_bytes().to_owned(),
            vec![0xff; 15],
            "3".as_bytes().to_owned(),
            vec![0xff; 2],
            "3".as_bytes().to_owned(),
        ]
        .into_iter()
        .flatten()
        .collect();
        assert_eq!(binfile.to_bytes(.., None), Ok(data));
        assert_eq!(binfile.segments().len(), 3);

        let mut binfile = BinFile::new();
        assert!(binfile
            .add_bytes("111111".as_bytes(), Some(8), false)
            .is_ok());
        assert_eq!(binfile.exclude(4..2), Err(Error::InvalidAddressRange));

        assert!(binfile.exclude(2..2).is_ok());
        assert_eq!(
            binfile.to_bytes(.., None),
            Ok("111111".as_bytes().to_owned())
        );
    }

    #[test]
    fn test_crop() {
        let mut binfile = BinFile::new();
        assert!(binfile.add_file("tests/in.s19", false).is_ok());
        assert!(binfile.crop(2..4).is_ok());
        let data = open_text_file("tests/in_crop_2_4.s19");
        assert_eq!(
            binfile.to_srec(Some(32), SRecordAddressLength::Length16),
            Ok(data)
        );
        assert!(binfile.exclude(2..4).is_ok());
        assert_eq!(binfile.to_bytes(.., Some(0x00)), Ok(vec![]));
    }

    #[test]
    fn text_minimum_maximum_length() {
        let mut binfile = BinFile::new();

        // Get values of an empty file
        assert_eq!(binfile.minimum_address(), None);
        assert_eq!(binfile.maximum_address(), None);
        assert_eq!(binfile.len(), 0);
        assert!(binfile.is_empty());

        // Read in small file
        assert!(binfile.add_file("tests/in.s19", false).is_ok());

        assert_eq!(binfile.minimum_address(), Some(0));
        assert_eq!(binfile.maximum_address(), Some(70));
        assert_eq!(binfile.len(), 70);
        assert!(!binfile.is_empty());

        // Add second segment
        assert!(binfile.add_bytes([0x01; 9], Some(80), false).is_ok());

        assert_eq!(binfile.minimum_address(), Some(0));
        assert_eq!(binfile.maximum_address(), Some(89));
        assert_eq!(binfile.len(), 79);
        assert!(!binfile.is_empty());
    }

    #[test]
    fn test_iterate_segments() {
        let binfile = BinFile::from_file("tests/in.s19").unwrap();

        let mut i = 0;
        for _segment in binfile.segments() {
            i += 1;
        }
        assert_eq!(i, 1);
        assert_eq!(binfile.segments().len(), 1);
    }

    #[test]
    fn test_segments_list() {
        let mut binfile = BinFile::new();

        assert!(binfile.add_bytes([0x00], Some(0), false).is_ok());
        assert!(binfile.add_bytes([0x01, 0x02], Some(10), false).is_ok());
        assert!(binfile.add_bytes([0x03], Some(12), false).is_ok());
        assert!(binfile.add_bytes([0x04], Some(1000), false).is_ok());

        assert_eq!(
            binfile.segments_list(),
            vec![
                (0, vec![0x00]),
                (10, vec![0x01, 0x02, 0x03]),
                (1000, vec![0x04]),
            ]
        )
    }

    #[test]
    fn test_chunks_list() {
        let mut binfile = BinFile::new();

        assert!(binfile
            .add_bytes([0x00, 0x00, 0x01, 0x01, 0x02], Some(0), false)
            .is_ok());
        assert!(binfile
            .add_bytes([0x04, 0x05, 0x05, 0x06, 0x06, 0x07], Some(9), false)
            .is_ok());
        assert!(binfile.add_bytes([0x09], Some(19), false).is_ok());
        assert!(binfile.add_bytes([0x0a], Some(21), false).is_ok());

        assert_eq!(
            binfile.to_bytes(.., None),
            Ok(vec![
                0x00, 0x00, 0x01, 0x01, 0x02, 0xff, 0xff, 0xff, 0xff, 0x04, 0x05, 0x05, 0x06, 0x06,
                0x07, 0xff, 0xff, 0xff, 0xff, 0x09, 0xff, 0x0a
            ])
        );

        // Size 8, alignment 1
        let mut data = BTreeMap::new();
        data.insert(0, vec![0x00, 0x00, 0x01, 0x01, 0x02]);
        data.insert(9, vec![0x04, 0x05, 0x05, 0x06, 0x06, 0x07]);
        data.insert(19, vec![0x09]);
        data.insert(21, vec![0x0a]);
        assert_eq!(binfile.segments().chunks(Some(8), None), Ok(data));

        // Size 8, alignment 2
        let mut data = BTreeMap::new();
        data.insert(0, vec![0x00, 0x00, 0x01, 0x01, 0x02]);
        data.insert(9, vec![0x04]);
        data.insert(10, vec![0x05, 0x05, 0x06, 0x06, 0x07]);
        data.insert(19, vec![0x09]);
        data.insert(21, vec![0x0a]);
        assert_eq!(binfile.segments().chunks(Some(8), Some(2)), Ok(data));

        // Size 8, alignment 4
        let mut data = BTreeMap::new();
        data.insert(0, vec![0x00, 0x00, 0x01, 0x01, 0x02]);
        data.insert(9, vec![0x04, 0x05, 0x05]);
        data.insert(12, vec![0x06, 0x06, 0x07]);
        data.insert(19, vec![0x09]);
        data.insert(21, vec![0x0a]);
        assert_eq!(binfile.segments().chunks(Some(8), Some(4)), Ok(data));

        // Size 8, alignment 8
        let mut data = BTreeMap::new();
        data.insert(0, vec![0x00, 0x00, 0x01, 0x01, 0x02]);
        data.insert(9, vec![0x04, 0x05, 0x05, 0x06, 0x06, 0x07]);
        data.insert(19, vec![0x09]);
        data.insert(21, vec![0x0a]);
        assert_eq!(binfile.segments().chunks(Some(8), Some(8)), Ok(data));

        // Size 4, alignment 1
        let mut data = BTreeMap::new();
        data.insert(0, vec![0x00, 0x00, 0x01, 0x01]);
        data.insert(4, vec![0x02]);
        data.insert(9, vec![0x04, 0x05, 0x05, 0x06]);
        data.insert(13, vec![0x06, 0x07]);
        data.insert(19, vec![0x09]);
        data.insert(21, vec![0x0a]);
        assert_eq!(binfile.segments().chunks(Some(4), None), Ok(data));

        // Size 4, alignment 2
        let mut data = BTreeMap::new();
        data.insert(0, vec![0x00, 0x00, 0x01, 0x01]);
        data.insert(4, vec![0x02]);
        data.insert(9, vec![0x04]);
        data.insert(10, vec![0x05, 0x05, 0x06, 0x06]);
        data.insert(14, vec![0x07]);
        data.insert(19, vec![0x09]);
        data.insert(21, vec![0x0a]);
        assert_eq!(binfile.segments().chunks(Some(4), Some(2)), Ok(data));

        // Size 4, alignment 4
        let mut data = BTreeMap::new();
        data.insert(0, vec![0x00, 0x00, 0x01, 0x01]);
        data.insert(4, vec![0x02]);
        data.insert(9, vec![0x04, 0x05, 0x05]);
        data.insert(12, vec![0x06, 0x06, 0x07]);
        data.insert(19, vec![0x09]);
        data.insert(21, vec![0x0a]);
        assert_eq!(binfile.segments().chunks(Some(4), Some(4)), Ok(data));
    }

    #[test]
    fn test_segment() {
        let mut binfile = BinFile::new();
        assert!(binfile
            .add_bytes([0x00, 0x01, 0x02, 0x03, 0x04], Some(2), false)
            .is_ok());

        // size 4, alignment 4
        let mut data = BTreeMap::new();
        data.insert(2, vec![0x00, 0x01]);
        data.insert(4, vec![0x02, 0x03, 0x04]);
        assert_eq!(binfile.segments()[0].chunks(Some(4), Some(4)), Ok(data));

        // Bad argument
        assert_eq!(
            binfile.segments()[0].chunks(Some(4), Some(8)),
            Err(Error::AlignmentToSizeError {
                size: 4,
                alignment: 8
            })
        );

        // Missing segment
        assert!(
            std::panic::catch_unwind(|| binfile.segments()[1].chunks(Some(4), Some(4))).is_err()
        );
    }

    #[test]
    fn test_add_files() {
        let mut binfile = BinFile::new();
        assert!(binfile.add_bytes([0], None, false).is_ok());

        let mut binfile_1_2 = BinFile::new();
        assert!(binfile_1_2.add_bytes([1], Some(1), false).is_ok());

        binfile += binfile_1_2;
        assert_eq!(binfile.to_bytes(.., None), Ok(vec![0, 1]));
    }

    #[test]
    fn test_info() {
        let binfile = BinFile::try_from(PathBuf::from("tests/empty_main.s19")).unwrap();
        let data = r#"Header:                  "bincopy/empty_main.s19"
Execution start address: 0x00400400
Data ranges:

    0x00400238 - 0x004002b4 (124 B)
    0x004002b8 - 0x0040033e (134 B)
    0x00400340 - 0x004003c2 (130 B)
    0x004003d0 - 0x00400572 (418 B)
    0x00400574 - 0x0040057d (9 B)
    0x00400580 - 0x004006ac (300 B)
    0x00600e10 - 0x00601038 (552 B)
"#;
        assert_eq!(binfile.info(), data);
    }

    #[test]
    fn test_layout_empty_main() {
        let binfile = BinFile::try_from(PathBuf::from("tests/empty_main.s19")).unwrap();
        let data = r###"0x400238                                                                0x601038
-                                                                              -"###;
        assert_eq!(binfile.layout(), Ok(data.to_string()));
    }

    #[test]
    fn test_layout_out() {
        let binfile = BinFile::try_from(PathBuf::from("tests/out.hex")).unwrap();
        let data = r###"0x0                                                                        0x403
=====-               -====-                                                    -"###;
        assert_eq!(binfile.layout(), Ok(data.to_string()));
    }

    #[test]
    fn test_layout_in_exclude_2_4() {
        let binfile = BinFile::try_from(PathBuf::from("tests/in_exclude_2_4.s19")).unwrap();
        let data = r###"0x0                                                               0x46
==  =================================================================="###;
        assert_eq!(binfile.layout(), Ok(data.to_string()));
    }

    #[test]
    fn test_execution_start_address() {
        let mut binfile = BinFile::try_from(PathBuf::from("tests/empty_main.s19")).unwrap();
        assert_eq!(binfile.execution_start_address(), Some(0x00400400));
        binfile.set_exexution_start_address(0x00400401);
        assert_eq!(binfile.execution_start_address(), Some(0x00400401));
    }

    #[test]
    fn test_add_ihex_record_type_3() {
        let mut binfile = BinFile::new();
        assert!(binfile.add_ihex([":0400000302030405EB"], false).is_ok());
        assert_eq!(binfile.execution_start_address(), Some(0x02030405));
    }

    #[test]
    fn test_add_ihex_record_type_5() {
        let mut binfile = BinFile::new();
        assert!(binfile.add_ihex([":0400000501020304ED"], false).is_ok());
        assert_eq!(binfile.execution_start_address(), Some(0x01020304));
    }

    #[test]
    fn test_add_ihex_record_type_6() {
        let mut binfile = BinFile::new();
        assert_eq!(
            binfile.add_ihex([":00000006FA"], false),
            Err(Error::UnsupportedRecordType {
                record: ":00000006FA".into(),
                record_type: 6
            })
        );
    }

    #[test]
    fn test_add_to_srec_record_5() {
        let mut binfile = BinFile::new();
        assert!(binfile.add_bytes([0x00; 65535], None, false).is_ok());
        let records = binfile.to_srec(Some(1), SRecordAddressLength::default());
        assert!(records.is_ok());
        assert_eq!(records.clone().unwrap().len(), 65536);
        assert_eq!(records.unwrap().last(), Some(&"S503FFFFFE".to_string()));
    }

    #[test]
    fn test_add_to_srec_record_6() {
        let mut binfile = BinFile::new();
        assert!(binfile.add_bytes([0x00; 65536], None, false).is_ok());
        let records = binfile.to_srec(Some(1), SRecordAddressLength::default());
        assert!(records.is_ok());
        assert_eq!(records.clone().unwrap().len(), 65537);
        assert_eq!(records.unwrap().last(), Some(&"S604010000FA".to_string()));
    }

    #[test]
    fn test_add_to_srec_record_8() {
        let mut binfile = BinFile::new();
        assert!(binfile.add_bytes([0x00], None, false).is_ok());
        binfile.set_exexution_start_address(0x123456);
        assert_eq!(
            binfile.to_srec(Some(1), SRecordAddressLength::Length24),
            Ok(vec![
                "S20500000000FA".to_string(),
                "S5030001FB".to_string(),
                "S8041234565F".to_string()
            ])
        );
    }

    #[test]
    fn test_issue_4_1() {
        let mut binfile = BinFile::new();
        assert!(binfile.add_ihex_file("tests/issue_4_in.hex", false).is_ok());
        let data = open_text_file("tests/issue_4_out.hex");
        assert_eq!(binfile.to_ihex(None, IHexFormat::default()), Ok(data));
    }

    #[test]
    fn test_issue_4_2() {
        let mut binfile = BinFile::new();
        assert!(binfile.add_srec_file("tests/empty_main.s19", false).is_ok());
        let data = open_text_file("tests/empty_main.hex");
        assert_eq!(binfile.to_ihex(None, IHexFormat::default()), Ok(data));
    }

    #[test]
    fn test_overwrite() {
        let mut binfile = BinFile::new();

        // Overwrite in empty file
        assert!(binfile
            .add_bytes("1234".as_bytes(), Some(512), true)
            .is_ok());
        assert_eq!(
            binfile.to_bytes(512.., None),
            Ok("1234".as_bytes().to_vec())
        );

        // Test setting data with multiple existing segments.
        assert!(binfile
            .add_bytes("123456".as_bytes(), Some(1024), false)
            .is_ok());
        assert!(binfile.add_bytes("99".as_bytes(), Some(1026), true).is_ok());
        let data = vec![
            "1234".as_bytes().to_owned(),
            vec![0xff; 508],
            "129956".as_bytes().to_owned(),
        ]
        .into_iter()
        .flatten()
        .collect();
        assert_eq!(binfile.to_bytes(512.., None), Ok(data));

        // Test setting data crossing the original segment limits.
        assert!(binfile
            .add_bytes("abc".as_bytes(), Some(1022), true)
            .is_ok());
        assert!(binfile
            .add_bytes("def".as_bytes(), Some(1029), true)
            .is_ok());
        let data = vec![
            "1234".as_bytes().to_owned(),
            vec![0xff; 506],
            "abc2995def".as_bytes().to_owned(),
        ]
        .into_iter()
        .flatten()
        .collect();
        assert_eq!(binfile.to_bytes(512.., None), Ok(data));

        // Overwrite a segment and write outside it.
        assert!(binfile
            .add_bytes("111111111111".as_bytes(), Some(1021), true)
            .is_ok());
        let data = vec![
            "1234".as_bytes().to_owned(),
            vec![0xff; 505],
            "111111111111".as_bytes().to_owned(),
        ]
        .into_iter()
        .flatten()
        .collect();
        assert_eq!(binfile.to_bytes(512.., None), Ok(data));

        // Overwrite multiple segments (all segments in this test).
        assert!(binfile
            .add_bytes(["1".as_bytes()[0]; 1204], Some(256), true)
            .is_ok());
        assert_eq!(
            binfile.to_bytes(256.., None),
            Ok(vec!["1".as_bytes()[0]; 1204])
        );
    }

    #[test]
    fn test_non_sorted_segments() {
        let mut binfile = BinFile::new();
        assert!(binfile
            .add_srec_file("tests/non_sorted_segments.s19", false)
            .is_ok());
        let data = open_text_file("tests/non_sorted_segments_merged_and_sorted.s19");
        assert_eq!(
            binfile.to_srec(None, SRecordAddressLength::default()),
            Ok(data)
        );
    }

    #[test]
    fn test_fill() {
        let mut binfile = BinFile::new();

        // Fill empty file
        assert!(binfile.fill(None, None).is_ok());
        assert!(binfile.is_empty());

        // Add some data and fill again
        assert!(binfile
            .add_bytes([0x01, 0x02, 0x03, 0x04], Some(0), false)
            .is_ok());
        assert!(binfile
            .add_bytes([0x01, 0x02, 0x03, 0x04], Some(8), false)
            .is_ok());
        assert!(binfile.fill(None, None).is_ok());
        assert_eq!(
            binfile.to_bytes(.., None),
            Ok(vec![
                0x01, 0x02, 0x03, 0x04, 0xff, 0xff, 0xff, 0xff, 0x01, 0x02, 0x03, 0x04
            ])
        );
    }

    #[test]
    fn test_fill_max_words() {
        let mut binfile = BinFile::new();

        // Add some data and fill again
        assert!(binfile.add_bytes([0x01], Some(0), false).is_ok());
        assert!(binfile.add_bytes([0x02], Some(2), false).is_ok());
        assert!(binfile.add_bytes([0x03], Some(5), false).is_ok());
        assert!(binfile.add_bytes([0x04], Some(9), false).is_ok());
        assert!(binfile.fill(Some(0xaa), Some(2)).is_ok());
        assert_eq!(binfile.segments().len(), 2);
        assert_eq!(binfile.segments()[0].minimum_address(), 0);
        assert_eq!(
            binfile.segments()[0].data(),
            &[0x01, 0xaa, 0x02, 0xaa, 0xaa, 0x03]
        );
        assert_eq!(binfile.segments()[1].minimum_address(), 9);
        assert_eq!(binfile.segments()[1].data(), &[0x04]);
    }

    #[test]
    fn test_get_value_by_address() {
        let mut binfile = BinFile::new();
        assert!(binfile
            .add_bytes([0x01, 0x02, 0x03, 0x04], Some(1), false)
            .is_ok());
        assert!(binfile.get_value_by_address(0).is_none());
        assert_eq!(binfile.get_value_by_address(1), Some(1));
        assert_eq!(binfile.get_value_by_address(2), Some(2));
        assert_eq!(binfile.get_value_by_address(3), Some(3));
        assert_eq!(binfile.get_value_by_address(4), Some(4));
        assert!(binfile.get_value_by_address(5).is_none());
    }

    #[test]
    fn test_get_values_by_address_ranges() {
        let mut binfile = BinFile::new();
        assert!(binfile
            .add_bytes([0x01, 0x02, 0x03, 0x04], Some(1), false)
            .is_ok());
        assert!(binfile
            .add_bytes([0x01, 0x02, 0x03, 0x04], Some(11), false)
            .is_ok());
        assert_eq!(
            binfile.get_values_by_address_range(3..5),
            Some(vec![0x03, 0x04])
        );
        assert!(binfile.get_values_by_address_range(3..6).is_none());
        assert!(binfile.get_values_by_address_range(0..3).is_none());
        assert_eq!(
            binfile.get_values_by_address_range(3..),
            Some(vec![0x03, 0x04])
        );
        assert_eq!(
            binfile.get_values_by_address_range(..3),
            Some(vec![0x01, 0x02])
        );
        assert_eq!(
            binfile.get_values_by_address_range(11..15),
            Some(vec![0x01, 0x02, 0x03, 0x04])
        );

        assert_eq!(
            binfile.get_values_by_address_range(13..15),
            Some(vec![0x03, 0x04])
        );
        assert!(binfile.get_values_by_address_range(13..16).is_none());
        assert!(binfile.get_values_by_address_range(10..13).is_none());
        assert_eq!(
            binfile.get_values_by_address_range(13..),
            Some(vec![0x03, 0x04])
        );
        assert_eq!(
            binfile.get_values_by_address_range(..13),
            Some(vec![0x01, 0x02])
        );
        assert_eq!(
            binfile.get_values_by_address_range(11..15),
            Some(vec![0x01, 0x02, 0x03, 0x04])
        );
    }

    #[test]
    fn test_performance() {
        let mut binfile = BinFile::new();

        // add a 1MB consecuitive binary
        let chunk = vec!["1".as_bytes()[0]; 1024];
        for i in 0..1024 {
            assert!(binfile
                .add_bytes(chunk.clone(), Some(1024 * i), false)
                .is_ok());
        }
        assert_eq!(binfile.minimum_address(), Some(0));
        assert_eq!(binfile.maximum_address(), Some(1024 * 1024));

        let ihex = binfile.to_ihex(None, IHexFormat::default()).unwrap();
        let srec = binfile
            .to_srec(None, SRecordAddressLength::default())
            .unwrap();

        binfile = BinFile::new();
        assert!(binfile.add_ihex(ihex, false).is_ok());

        binfile = BinFile::new();
        assert!(binfile.add_srec(srec, false).is_ok());
    }

    #[test]
    fn test_print() {
        let mut binfile = BinFile::new();

        assert!(binfile.add_bytes([0, 1, 2], Some(0), false).is_ok());
        assert!(binfile.add_bytes([3, 4, 5], Some(10), false).is_ok());

        for segment in binfile.segments() {
            println!("{}", segment);
            for (address, data) in segment.chunks(Some(2), None).unwrap() {
                println!("Adress: {} Data: {:?}", address, data);
            }
        }
    }

    #[test]
    fn test_verilog_vmem() {
        let mut binfile = BinFile::new();

        assert!(binfile.add_verilog_vmem(
             [
             "@00000100 21 46 01 36 01 21 47 01 36 00 7E FE 09 D2 19 01 21 46 01 7E 17 C2 00 01 FF 5F 16 00 21 48 01 19",
             "@00000120 19 4E 79 23 46 23 96 57 78 23 9E DA 3F 01 B2 CA 3F 01 56 70 2B 5E 71 2B 72 2B 73 21 46 01 34 21",
             ], false).is_ok());
    }
}
